<div class="page-title parallax parallax4" style=" background-repeat: no-repeat; background-position: 50% 0px; background-image: url(../../img/parallax/bg-parallax4_7.jpg);  background-size: inherit;" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Contactar</h2>
                </div><!-- /.page-title-heading -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><a href="<?= site_url() ?>">Home</a></li>
                        <li>Contacte</li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title parallax -->

<section class="flat-row portfolio-row-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="contact-widget">
                    <div class="widget contact-info">
                        <h4 class="widget-title">Aquí Estem</h4>
                        <ul>
                            <li class="address"><a href="#">Avda. Mestre Montaner, 86</a></li>
                            <li class="address1"><a href="#">08700 Igualada, BARCELONA</a></li>
                            <li class="phone"><a href="#">938 03 57 62</a></li>

                            <li class="email"><a href="#">info@futurmod.fashion</a></li>  
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-4 -->
            <div class="col-md-8">
                <div class="widget-contactform">
                    <form method="post" action="<?= base_url('paginas/frontend/contacto') ?>?redirect=<?= base_url('p/contacto') ?>">
                        <div class="row">
                            <div class="col-md-6">
                                <p><input id="name" name="nom" type="text" value="" placeholder="Nom" required="required"></p>
                                <p><input id="email" name="email" type="email" value="" placeholder="Email" required="required"></p>
                                <p><input id="website" name="web" type="text" value="" placeholder="Web"></p>
                                <p><input id="phone" name="telefon" type="text" value="" placeholder="Telèfon" required="required"></p>                                
                            </div><!-- /.col-md-6 -->

                            <div class="col-md-6">
                                <?php if (!empty($_SESSION['msj'])) echo $_SESSION['msj'] ?>
                                <p>
                                    <textarea name="message" placeholder="Comentari" required="required"><?= !empty($_GET['proyecto'])?'Desitjo més informació del projecte #'.$_GET['proyecto']:'' ?></textarea>
                                </p>
                                <p>
                                    <div class="g-recaptcha" data-sitekey="6LeEGCcUAAAAAJtGSuRh-JQPbzosXtTIoyHIjmEE" style="
    margin-bottom: 34.2px;
"></div>
                                </p>
                                <span class="form-submit">
                                    <input name="submit" type="submit" id="submit" class="submit" value="Enviar Email">
                                </span>
                            </div><!-- /.col-md-6 -->
                        </div><!-- /.row -->
                    </form>
                </div><!-- /.col-md-8 -->
            </div><!-- /.widget-contactform -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /flat-row portfolio-row-page -->

<div id="flat-map" data-zoom='15' data-lat="41.5844191" data-lon="1.6278959999999643" data-direccion="Avda. Mestre Montaner, 86. 08700 Igualada, Barcelona" data-icon="<?= base_url('img/marker.png') ?>">
</div><!-- /#flat-map -->
<?php $_SESSION['msj'] = null ?>
