 <div class="page-title parallax parallax4 seminaris" style=" background-position: 50% 49px; background-image: url(../../img/parallax/bg-parallax4_5.jpg);  background-size: inherit;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading style1">
                    <h2 class="title">Seminari</h2>
                </div><!-- /.page-title-heading -->
                <div class="breadcrumbs style1">
                    <ul>
                        <li class="home"><a href="#">Home</a></li>
                        <li class="home"><a href="#">Seminari</a></li>
                        <li>Detall Seminari</li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title parallax -->

<section class="main-content blog-post v1">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="post-wrap">
                    <article class="post">
                        <div class="entry-image">
                            <img src="<?= $detail->foto ?>" alt="image" style="width:100%">
                        </div>
                        <div class="content-post">
                            <div class="title-education">
                                <p><?= $detail->categorias->row(0)->nombre ?></p>
                            </div>
                            <h4 class="title-post">
                                <a href="#"><?= $detail->titulo ?></a>
                            </h4>
                            <div class="entry-meta">                              
                                <span class="author"><a href="#"><?= $detail->user ?></a></span>
                                <span class="date"><a href="#"><?= date("d-m-Y",strtotime($detail->fecha)) ?> <?= $detail->hora ?></a></span>                                
                            </div><!-- /.entry-meta -->

                            <div class="entry-content">
                                <?= $detail->texto ?>
                            </div><!-- /entry-post -->

                            <ul class="flat-socials flat-left">
                                <?php if(!empty($detail->presentacion2)): ?>
                                <li class="facebook">
                                    <a href="<?= base_url('files/'.$detail->presentacion2) ?>" target="_new"><button type="button" id="buttonid24" class="flat-button button-style" style="font-size: 70%; padding: 6px 10px;"><i class="fa fa-file-pdf-o"></i> Veure presentació</button></a>
                                </li>
                                <?php endif ?>
                                <?php if(!empty($detail->presentacion)): ?>
                                <li class="facebook">
                                    <a href="<?= base_url('files/'.$detail->presentacion) ?>" target="_new"><button type="button" id="buttonid24" class="flat-button button-style" style="font-size: 70%; padding: 6px 10px;"><i class="fa fa-file-pdf-o"></i> Veure programa</button></a>
                                </li>
                                <?php endif ?>
                                <?php if(!empty($detail->formulario)): ?>
                                <li class="facebook">
                                    <a href="<?= base_url('reservas/'.$detail->formulario.'-Formulario de reserva') ?>" target="_new"><button type="button" id="buttonid24" class="flat-button button-style" style="font-size: 70%; padding: 6px 10px;"><i class="fa fa-file"></i> Inscriu-te</button></a>
                                </li>
                                <?php endif ?>
                         
                            </ul>

                            <ul class="flat-socials">
                                <li class="facebook">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= site_url('proyectes/'.toUrl($detail->id.'-'.$detail->titulo)) ?>">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="twitter">
                                    <a href="https://twitter.com/share?text=<?= $detail->titulo ?>&url=<?= site_url('proyectes/'.toUrl($detail->id.'-'.$detail->titulo)) ?>">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                         
                            </ul>
                        </div><!-- /content-post -->
                    </article>
                    
                </div><!-- /post-wrap -->
            </div><!-- /col-md-8 -->

            <div class="col-md-4">
                <div class="sidebar">
                    <div class="widget widget-search">
                        <form action="<?= base_url('seminaris') ?>" id="searchform" method="get">
                            <div>
                                <input type="text" id="s" name="direccion" class="sss" placeholder="Buscar">
                                <input type="hidden" id="page" name="page" value="<?= !empty($_GET['page'])?$_GET['page']:'1' ?>">
                                <input type="hidden" id="categorias_proyectos_id" name="categorias_proyectos_id" value="<?= !empty($_GET['categorias_proyectos_id'])?$_GET['categorias_proyectos_id']:'' ?>">
                                <input type="submit" value="" id="searchsubmit">
                            </div>
                        </form>
                    </div><!-- /widget-search -->

                    <div class="widget widget-recent-posts">
                        <h5 class="widget-title">Seminaris Relacionats</h5>
                        <ul class="recent-posts clearfix">
                            <?php foreach($relacionados->result() as $r): ?>
                            <li>
                                <div class="thumb">
                                    <img src="<?= $r->foto ?>" alt="image" style="width:84px">
                                </div>
                                <div class="text">
                                    <a href="<?= $r->link ?>"><?= $r->titulo ?></a>
                                    <p><?= strftime("%A, %d-%b-%Y",strtotime($r->fecha)) ?></p>
                                </div>
                            </li>
                            <?php endforeach ?>
                            <?php if($relacionados->num_rows()==0): ?>
                              <li class="item-recent clearfix">
                                Sense articles relacionats
                              </li>
                            <?php endif ?>
                        </ul><!-- /popular-news clearfix -->
                    </div><!-- /widget widget-recent-posts -->
                    
                </div><!-- /sidebar -->
            </div><!-- /col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->   
</section><!-- /main-content blog-post -->
<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchform").submit();
    }
    
    function changeCategoria(id){
        $("#blog_categorias_id").val(id);
        $("#searchform").submit();
    }
</script>
