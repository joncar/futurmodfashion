<!-- Slider -->
<div class="tp-banner-container">
    <div class="tp-banner">
        <ul>
            <li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?= base_url() ?>img/slides/5.jpg" alt="slider-image"/>
                <div class="overlay"></div>

                <div class="tp-caption sfr title-slide box center color-white large style2" data-x="116" data-y="275" data-speed="1500" data-start="1500" data-easing="Sine.easeInOut">
                    <span>Tens</span> Bones Idees?<br><span>Nosaltres</span> t'ajudem</div>

                <a href="<?= site_url('p/futurmod') ?>" class="tp-caption sfl flat-button border-white start" data-x="116" data-y="464" data-speed="1500" data-start="1500" data-easing="Sine.easeInOut">
                    Llegir Més
                </a>                    
            </li>

            <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?= base_url() ?>img/slides/4.jpg" alt="slider-image"/>
                <div class="overlay"></div>

                <div class="tp-caption sfr title-slide box center color-white large style1" data-x="116" data-y="272" data-speed="1500" data-start="1500" data-easing="Sine.easeInOut">Ets Creatiu i t'agrada<br> el món<span> de la Moda</span></div>

                <a href="<?= site_url('p/futurmod') ?>" class="tp-caption sfl flat-button border-white start" data-x="116" data-y="464" data-speed="1500" data-start="1500" data-easing="Sine.easeInOut">
                    Llegir Més
                </a>

            </li>

            <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?= base_url() ?>img/slides/1.jpg" alt="slider-image"/>
                <div class="overlay"></div>

                <div class="tp-caption sfr title-slide box center color-white large style1" data-x="116" data-y="272" data-speed="1500" data-start="1500" data-easing="Sine.easeInOut">De les Petites Idees <br> fem grans<span> Projectes</span></div>

                <a href="<?= site_url('p/futurmod') ?>" class="tp-caption sfl flat-button border-white start" data-x="116" data-y="464" data-speed="1500" data-start="1500" data-easing="Sine.easeInOut">
                    Llegir Més
                </a>

            </li>

            <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?= base_url() ?>img/slides/2.jpg" alt="slider-image"/>
                <div class="overlay"></div>

                <div class="tp-caption sfb title-slide box center color-white large style2" data-x="116" data-y="272" data-speed="1500" data-start="1500" data-easing="Power3.easeInOut">
                    Ara és el moment per a <br> promocionar el teu <span>Projecte</span>
                </div>
                <a href="<?= site_url('p/futurmod') ?>" class="tp-caption sfl flat-button border-white start" data-x="116" data-y="464" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                    Llegir Més
                </a>

            </li>

            <li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?= base_url() ?>img/slides/3.jpg" alt="slider-image"/>
                <div class="overlay"></div>

                <div class="tp-caption sfb title-slide box center color-white large style2" data-x="116" data-y="272" data-speed="1500" data-start="1500" data-easing="Power3.easeInOut">
                Assessorament personal<br><span> per a casos</span> Reals</div>

                <a href="<?= site_url('p/futurmod') ?>" class="tp-caption sfl flat-button border-white start" data-x="116" data-y="464" data-speed="1500" data-start="1500" data-easing="Power1.easeInOut">
                    Llegir Més
                </a>
            </li>
        </ul>
    </div>
</div>

<div id="flat-event-style1" class="flat-row mag-top-120px no-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="">                    
                    <?php $this->db->order_by('priority','ASC'); ?>
                    <?php $this->db->where('fecha >=',date("Y-m-d")); ?>
                    <?php $eventos = $this->db->get('eventos_banner'); ?>
                    <div class="flat-event-style2" data-size="<?= $eventos->num_rows() ?>">                        
                        <?php foreach($eventos->result() as $e): ?>
                             <a href="<?= $e->link ?>">
                            <div class="item">
                                <div class="event-img">
                                   
                                        <div style="height:auto; display:inline-block;">
                                            <img src="<?= base_url('img/eventos_banner/'.$e->fondo) ?>" style="opacity:1; ">
                                        </div>
                                   
                                </div>
                            </div><!-- /.item -->
                             </a>
                        <?php endforeach ?>

                    </div><!-- /.flat-event-style1 -->
                </div><!-- /.padding-45px -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="">
                    <div class="flat-rotate">
                        <div class="text-left">
                            <p>FORMA PART AVUI! </p>
                        </div>
                    </div>
                    <div class="flat-event-style1">
                        <div class="item">
                            <div class="event-img">
                                <div style="opacity:0.8; height:450px; background:url() no-repeat; background-size:cover; background-position:center">
                                    <img src="http://futurmod.fashion/img/about/11.jpg">
                                </div>
                            </div>
                            <div class="event-title">
                                <h5 class="title-1">Missió de Futur Mod</h5>
                                <h4 class="title">
                                    <a href="http://www.futurmod.fashion/p/futurmod.html">
                                        T'ajudem a impulsar <br>les teves idees
                                    </a>
                                </h4>
                            </div>
                            <div class="time-place">
                                <div class="flat-time">
                                    <i class="fa fa-info-circle"></i>&nbsp;
                                    <span class="month">Informa't</span>
                          <!--          <span class="day">01</span>,
                                    <span class="hours">08:00 AM</span>-->
                                </div>
                                <div class="event-add">
                                    <p class="add"><i class="fa fa-map-marker"></i>&nbsp;&nbsp; Acollida, Mentoring, Seminaris, Formacions, Recomenacions, Tallers, Visites... </p>
                                </div>
                                <div class="button-center">
                                    <button class="flat-button button-style"onclick="document.location.href='<?= base_url('p/futurmod.html') ?>'">Llegir més</button>
                                </div>
                            </div><!-- /.time-place -->
                        </div><!-- /.item -->

                        <div class="item">
                            <div class="event-img">
                                <div style="opacity:0.8; height:450px; background:url() no-repeat; background-size:cover; background-position:center">
                                    <img src="http://futurmod.fashion/img/about/22.jpg">
                                </div>
                            </div>
                            <div class="event-title">
                                <h5 class="title-1">Currículums</h5>
                                <h4 class="title">
                                    <a href="http://www.futurmod.fashion/p/pujar-curriculum">
                                        Envia'ns el teu <br>currículum
                                    </a>
                                </h4>
                            </div>
                            <div class="time-place">
                                <div class="flat-time">
                                    <i class="fa fa-users"></i>&nbsp;
                                    <span class="month">T'assessorarem</span>
                                   <!-- <span class="day">01</span>,
                                    <span class="hours">08:00 AM</span>-->
                                </div>
                                <div class="event-add">
                                    <p class="add"><i class="fa fa-map-marke"></i>Analitzem el teu CV i un dels nostres mentors et donarà les claus <br>per al teu èxit.</p>
                                </div>
                                <div class="button-center">
                                    <button class="flat-button button-style"onclick="document.location.href='<?= base_url('p/pujar-curriculum') ?>'">Enviar CV</button>
                                </div>
                            </div><!-- /.time-place -->
                        </div><!-- /.item -->

                        <div class="item">
                            <div class="event-img">
                                <div style="opacity:0.8; height:450px; background:url() no-repeat; background-size:cover; background-position:center">
                                    <img src="http://futurmod.fashion/img/about/33.jpg">
                                </div>
                            </div>
                            <div class="event-title">
                                <h5 class="title-1">Projectes</h5>
                                <h4 class="title">
                                    <a href="http://www.futurmod.fashion/registro/index/add">
                                        Puja el teu projecte<br> tu mateix!
                                    </a>
                                </h4>
                            </div>
                            <div class="time-place">
                                <div class="flat-time">
                                    <i class="fa fa-clock-o"></i>&nbsp;
                                    <span class="month">No esperis més</span>
                                   <!-- <span class="day">01</span>,
                                    <span class="hours">08:00 AM</span>-->
                                </div>
                                <div class="event-add">
                                    <p class="add"><i class="fa fa-map-marke"></i>Fes clik, dona't d'alta i puja el teu <br>projecte posa tota la informació que vulguis.</p>
                                </div>
                                <div class="button-center">
                                    <button class="flat-button button-style"onclick="document.location.href='<?= base_url('registro/index/add') ?>'">Pujar Projecte</button>
                                </div>
                            </div><!-- /.time-place -->
                        </div><!-- /.item -->

                    </div><!-- /.flat-event-style1 -->
                </div><!-- /.padding-45px -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.flat-row -->



<!--<div id="flat-event-style1" class="flat-row mag-top-120px no-padding">
    <div class="container">
        <div class="causes causess">
            <div class="causes-img">
                <img src="img/about/a.jpg" alt="image">
            </div>
            <div class="causes-post">
                <div class="title-education">
                    <p>PRESENTACIÓ DEL PROJECTE FUTUR MOD</p>
                </div>
                <h4 class="title-post" style="font-size: 32px;line-height: 35px;" >
                    <a>Ets emprenedor/ra i acabes d’engegar el teu propi negoci en el sector de la moda?</a>
                </h4>
                <div class="entry-content">
                    <p>FUTUR MOD és un projecte orientat a donar suport a emprenedors i micropimes de nova creació en el sector de la moda, per facilitar la seva consolidació i creixement.</p><br>

                    <div class="">

                        <a> T’ho expliquem,
                            el proper<b> dimecres, 5 de juliol de 2017 a les 10:00 h <br>
                            a la seu del Clúster Tèxtil de Catalunya, MODACC, </b><br>C/ Milà i Fontanals, 14-26,
                            Barcelona. <br>
                           </a> Contacte: Àngels Bernaus,<a href="Tel:938032993" style="text-decoration: underline; color:darkgray"> Tel. 93 803 29 93</a>, <spam>o </spam>      <a href="mailto:angels@fagepi.net" style="text-decoration: underline;color:darkgray">  e-mail: angels@fagepi.net </a>


                    </div>
                </div>
                <div class="causes-button">
                    <a href="https://docs.google.com/forms/d/1F0mZmStKBQH_5iW-TC2Hu7pgOj9bxjCYwfhvE2OqYig/edit"><button class="flat-button button-style style1">Registra't</button></a>
                    <p></p>
                    <a href="https://www.google.es/maps/place/Modacc+Cl%C3%BAster/@41.4006141,2.1630586,3a,75y,90t/data=!4m8!1m2!2m1!1sClu%CC%81ster+Te%CC%80xtil+de+Catalunya,+MODACC,+C%2F+Mila%CC%80+i+Fontanals,+14-26,+Barcelona!3m4!1s0x12a4a2c2ac13dbaf:0x9064eed36e8a6025!8m2!3d41.4006354!4d2.1630788"><button class="flat-button button-style style2">Com arribar</button></a>
                </div>
            </div>
        </div>
    </div>
</div>-->


<!-- Flat counter -->
<div class="page-title parallax parallax1">
        <div class="overlay"></div>            
        <div class="container">
            <div class="row">
                <div class="col-md-12">                    
                    <div class="page-title-heading">
                        <h2 class="title white">Vols impulsar el teu negoci?<br> Tens una startup?<br>Nosaltres t'ajudem</h2>
                    </div><!-- /.page-title-heading -->
                    <div class="parallax-button">
                        <button class="flat-button button-style" onclick="document.location.href='<?= base_url('p/pujar-curriculum') ?>'">Puja el teu Currículum</button>
                        <a href="<?= base_url('p/pujar-curriculum') ?>" style="display:none">Puja el teu Currículum</a>
                    </div>
                </div><!-- /.col-md-12 -->  
            </div><!-- /.row -->  
        </div><!-- /.container -->          
    </div><!-- /page-title parallax parallax1 -->

<section class="page-section pt-90 pb-80 bg-main pattern relative">
<div class="container">
  <div class="call-out-box clearfix with-icon">
    <div class="row call-out-wrap">
      <div class="col-md-5">
        <h6 class="title-section-top gray font-4">Subscriu-te avui</h6>
        <h2 class="title-sectionsubscribe alt-2"><span>Vols</span> estar informat?</h2>
        <i class="flaticon-suntour-email call-out-icon"></i>
      </div>
      <div class="col-md-7" style="padding-top:10px">
        <form onsubmit="return subscribir(this)" class="form contact-form mt-10" method="post" action="">
            <div id="footsubscralert"></div>
          <div class="input-container">
            <input type="email" class="newsletter-field mb-0 form-row" name="email" value="" placeholder="Escriu el teu email" id="email"><i class="flaticon-suntour-email icon-left"></i>
            <button class="subscribe-submit" type="submit">
                <i class="flaticon-suntour-arrow icon-right"></i>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</section>

<!-- recent causes -->
<section class="flat-row parallax parallax12 pad-top110px" id="projectes">
    <div class="overlay style1"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="title-section style1">
                    <h4 class="desc">Dóna a conèixer el teu projecte</h4>
                    <h2 class="title">Nous Emprenedors </h2>
                </div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>

<!-- Recent causes -->
<section id="projectess" class="flat-row flat-recent-causes recent-mag-top pad-bottom50px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="recent-causes style-v1">
                    
                    <?php foreach($proyectos->result() as $p): ?>                    
                        <?php if($p->categorias->num_rows()>0): ?>
                                <div class="post">
                                    <div class="wrap-post">
                                        <div class="img-recent-causes">
                                            <a href="<?= $p->link ?>"><img src="<?= $p->foto ?>" alt="image"></a>
                                        </div>
                                        <div class="post-recent-causes">
                                            <div class="title-later-new">
                                                <p> <?= $p->categorias->row(0)->nombre ?></p>
                                            </div> 
                                            <h6 class="title-post">
                                                <?php $ponts = strlen($p->titulo)>46?'...':''; ?>
                                                <a href="<?= $p->link ?>"><?= substr($p->titulo,0,46).$ponts ?></a>
                                            </h6>
                                        </div>
                                       <div class="entry-content">
                                            <div class="donation-v1">
                                                <p><?= cortar_palabras(strip_tags($p->descripcion_corta),45) ?></p><span></span>
                                            </div>

                                        </div>
                                        <div class="flat-progress">
                                            <div class="perc"><?= mb_strtolower($p->empresa,mb_detect_encoding($p->empresa)) ?></div>
                                            <div class="progress-bar" data-percent="100" data-waypoint-active="yes">
                                                <div class="progress-animate"></div>
                                            </div>
                                        </div><!-- /flat-progress -->
                                        <div class="more">
                                            <a class="visitas" title="visitas"><i class="icon-eye"></i><?= empty($p->visitas)?0:$p->visitas ?> <span class="hidden">vistas</span></a>
                                            <a class="likes" title="likes" id='likes<?= $p->id ?>' href="javascript:sumarCorazon('<?= $p->id ?>')"><i class="icon-heart"></i>  <?= $p->likes ?></a>
                                            <button onclick="document.location.href='<?= $p->link ?>'" class="flat-button button-style">Veure Projecte</button>
                                        </div>
                                    </div>                        
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                </div><!-- /post -->
                <!-- /recent-causes -->
            </div><!-- /col-md-12 -->
        </div><!-- /row -->
        <div class="flat-divider d50px"></div>
        <div class="row">
            <div class="button-center">
                <button class="flat-button button-style" onclick="document.location.href='<?= site_url('projectes') ?>'">Veure Més</button>
                <a href="<?= site_url('projectes') ?>" class="hidden">Veure Tots</a>
            </div>
        </div>
    </div><!-- /container -->
</section><!-- /flat-row flat-recent-causes -->   

<div class="flat-row page-title style1 parallax parallax13">
    <div class="overlay"></div>            
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title white">Dóna a conèixer el teu projecte,<br> fes que tothom el vegi!</h2>
                    <p class="desc">Si creus que el teu projecte es novedós, creatiu, diferent, interessant, puja'l aquí i explica'l amb tots els detalls que vulguis. Quant més complerta sigui la descripció més feedback tindràs. </p>
                    <button class="flat-button button-style" onclick='document.location.href="<?= site_url('panel') ?>"'>Puja'l aquí</button>
                </div><!-- /.page-title-heading -->
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->          
</div><!-- /page-title parallax parallax1 -->   

<!-- Flat event -->
<section class="flat-row pad-top50px" id="seminaris">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="title-section">
                    <h4>La formació al capdavant  </h4>
                    <h2><span></span> Seminaris</h2>
                </div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
        
            <?php $n = 0; ?>
            <?php foreach($seminarios->result() as $p): ?>
            <?php if($n==0): ?>
            <div class="row">
            <?php endif ?>
            <div class="col-md-4 col-sm-6">
                    <div class="flat-event">
                        <div class="event-img" style="background:url(<?= $p->foto ?>); background-size:cover; width:365px; height:266px;">
                            <img src="<?= $p->foto ?>" alt="images" style="display:none">
                        </div>
                        <div class="event-content">
                            <div class="flat-time">
                                <span class="day"><?= date("d",strtotime($p->fecha)) ?></span>
                                <span class="month"><?= strftime("%b",strtotime($p->fecha)) ?></span>
                                <span class="hours"><?= $p->hora ?></span> 
                            </div>
                            <div class="event-title">
                                <h4 class="title"><a href="<?= $p->link ?>"><?= $p->titulo ?></a></h4>
                                <p class="add"><i class="fa fa-map-marker"></i><?= $p->ubicacion ?></p>
                            </div>
                        </div>
                    </div>
                </div><!-- /col-md-4 -->
            <?php $n++; ?>
            <?php if($n==3): $n = 0; ?>
            </div><!-- /row -->
            <?php endif ?>
            <?php endforeach ?>
            <?php if($seminarios->num_rows()==0): ?>
            <div style="text-align:center">Estem preparant nous seminaris. Si vols veure les presentacions dels seminiaris que s’han fet ho pot fer <a href="<?= site_url('seminaris') ?>">aquí</a></div>
            <?php endif ?>
        <!--<div class="flat-divider d50px"></div>
        <div class="row">
            <div class="button-center">
                <button class="flat-button button-style" onclick="document.location.href='<?= site_url('seminaris') ?>'">Veure Més</button>
                <a href="<?= site_url('seminaris') ?>" class="hidden">Veure Més</a>
            </div>
        </div>-->
    </div><!-- /container -->
</section><!-- /flat-row flat-icon icon-left -->



<!-- Flat counter -->
<div class="page-title parallax parallax2">
    <div class="overlay v1"></div>            
    <div class="container">
        <div class="row">
            <div class="col-md-3">                    
                <div class="flat-counter">
                    <div class="numb-count" data-to="68" data-speed="2000" data-waypoint-active="yes">68</div>
                    <div class="name-count"><p>Emprenedors</p></div>
                </div><!-- /flat-counter -->
            </div><!-- /.col-md-3 -->  

            <div class="col-md-3">                    
                <div class="flat-counter">
                    <div class="numb-count" data-to="54" data-speed="2000" data-waypoint-active="yes">54</div>
                    <div class="name-count"><p>Empreses</p></div>
                </div><!-- /.roll-counter -->
            </div><!-- /.col-md-3 -->

            <div class="col-md-3">                    
                <div class="flat-counter counter">
                    <div class="numb-count" data-to="4" data-speed="2000" data-waypoint-active="yes">4</div>
                    <div class="name-count"><p>Mentoring</p></div>
                </div><!-- /flat-counter -->
            </div><!-- /.col-md-3 -->

            <div class="col-md-3">                    
                <div class="flat-counter">
                    <div class="numb-count" data-to="24" data-speed="2000" data-waypoint-active="yes">24</div>
                    <div class="name-count"><p>Seminaris</p></div>
                </div><!-- /flat-counter -->
            </div><!-- /.col-md-3 -->
        </div><!-- /.row -->  
    </div><!-- /.container -->          
</div><!-- /page-title parallax parallax2 -->

<section class=" flat-row partners-wrapper" id="empresas">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="title-section">
                    <h4>Treballem pel futur</h4>
                    <h2><span>Empreses</span> Consolidades</h2>
                </div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-10 col-lg-offset-1 clearfix">
                <div class="partners">
                    <div class="partner top">
                        <a href="http://www.escorpion.com"><img src="<?= base_url() ?>img/clients/1.jpg" alt="image"></a>
                    </div>
                    <div class="partner top">
                        <a href="http://www.puntoblanco.com"><img src="<?= base_url() ?>img/clients/2.jpg" alt="image"></a>
                    </div>
                    <div class="partner top">
                        <a href="https://www.sitamurt.com"><img src="<?= base_url() ?>img/clients/3.jpg" alt="image"></a>
                    </div>
                    <div class="partner top last">
                        <img src="<?= base_url() ?>img/clients/4.jpg" alt="image">
                    </div>
                    <div class="partner bottom">
                        <img src="<?= base_url() ?>img/clients/5.jpg" alt="image">
                    </div>
                    <div class="partner bottom">
                        <img src="<?= base_url() ?>img/clients/6.jpg" alt="image">
                    </div>
                    <div class="partner bottom">
                        <img src="<?= base_url() ?>img/clients/7.jpg" alt="image">
                    </div>
                    <div class="partner bottom last">
                        <img src="<?= base_url() ?>img/clients/8.jpg" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="mentors">
    <div class="overlay"></div>

    <div class="flat-testimonial style1">
        <ul class="slides testimonial clearfix">
            <li>
                <section class="flat-row flat-about-charry pad-bottom125px pad-top100px bg-colorf7f7f7"><a href="http://www.esdemarketing.com"</a>
                    <div class="container">
                        <div class="row">
                            <div class="title-section">
                                <h4>La formació ho és tot</h4>
                                <h2><span>Els</span> Mentors</h2>
                            </div>
                            <div class="about-charry">
                                <div class="col-md-6">
                                    <div class="img-single">
                                        <img src="<?= base_url() ?>img/about/1.png" alt="image">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="about-charry-post style1">
                                        <h4 class="title-post">
                                            <a href="http://www.esdemarketing.com">Llorenç Palomas</a>
                                        </h4>
                                        <p>Expert en desenvolupament de botigues online.</p>
                                        <p>Responsable del Departament de vendes i màrqueting de TRILOGI eCommerce, empresa referent i pionera en el desenvolupament de botigues online.
                                            Com a consultor, ha ajudat a incorporar en un món digital a més de 400 empreses i a potenciar les seves vendes off i online, orientant-les en temes de màrqueting digital i consultoria estratègica.</p>
                                        <div class="signature">
                                            <img src="<?= base_url() ?>img/about/firma1.png" alt="images">
                                        </div>
                                    </div>
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.about-charry -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>
            </li>
            
            <li>
                <section class="flat-row flat-about-charry pad-bottom125px pad-top100px bg-colorf7f7f7"> <a href="http://albertpallares.com"</a>
                    <div class="container">
                        <div class="row">
                            <div class="title-section">
                                <h4>La formació ho és tot</h4>
                                <h2><span>Els</span> Mentors</h2>
                            </div>
                            <div class="about-charry">
                                <div class="col-md-6">
                                    <div class="img-single">
                                        <img src="<?= base_url() ?>img/about/11.png" alt="image">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="about-charry-post style1">
                                        <h4 class="title-post">
                                            <a href="http://albertpallares.com">Albert Pallarès</a>
                                        </h4>
                                        <p>Consultor en comerç internacional per a PIMES </p>
                                        <p>"Experiència de 20 anys com a professional en Internacionalització de PIMES, i amb un extens coneixement del sector de la Moda en la seva vessant del posicionament del luxe Internacional. Professor en el Màster d'Internacionalització de Petita i Mitjana empresa de la Universitat de Barcelona."
                                        </p>
                                        <div class="signature">
                                            <img src="<?= base_url() ?>img/about/2albert.png" alt="images">
                                        </div>
                                    </div>
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.about-charry -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>
            </li>

            <li>
                <section class="flat-row flat-about-charry pad-bottom125px pad-top100px bg-colorf7f7f7"><a href="http://albertriba.com"</a>
                    <div class="container">
                        <div class="row">
                            <div class="title-section">
                                <h4>La formació ho és tot</h4>
                                <h2><span>Els</span> Mentors</h2>
                            </div>
                            <div class="about-charry">
                                <div class="col-md-6">
                                    <div class="img-single">
                                        <img src="<?= base_url() ?>img/about/111foster.png" alt="image">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="about-charry-post style1">
                                        <h4 class="title-post">
                                            <a href="http://albertriba.com">Markus Förster</a>
                                        </h4>
                                        <p>Alemany, enamorat de Barcelona des de 1992, enginyer industrial, amb trajectòria internacional de més de 20 anys  (9 anys com Director Comercial de la marca de moda infantil Bóboli) assessor en retail i moda, i en àrees d’expansió comercial, estratègia i transformació corporativa.
                                        </p>
                                        <p>Àmplia experiència com a inversor privat, cofundador i mentor en startups innovadores i disruptives i programes d’acceleració de startups. Membre de les xarxes IESE Private Investors’ Network i ESADE Business Angel Network.
                                            .
                                        </p>
                                        <div class="signature">
                                            <img src="<?= base_url() ?>img/about/firmafoster.png" alt="images">
                                        </div>
                                    </div>
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.about-charry -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>
            </li>

            <li>
                <section class="flat-row flat-about-charry pad-bottom125px pad-top100px bg-colorf7f7f7"><a href="http://www.fitex.es"</a>
                    <div class="container">
                        <div class="row">
                            <div class="title-section">
                                <h4>La formació ho és tot</h4>
                                <h2><span>Els</span> Mentors</h2>
                            </div>
                            <div class="about-charry">
                                <div class="col-md-6">
                                    <div class="img-single">
                                        <img src="<?= base_url() ?>img/about/1111.png" alt="image">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="about-charry-post style1">
                                        <h4 class="title-post">
                                            <a href="http://www.fitex.es">David Garcia</a>
                                        </h4>
                                        <p>Director Executiu de l’Agrupación Española del Género de Punto, Cluster Manager de MODACC (Clúster Tèxtil Català de la Moda), Director General de la Fundació per a la Innovació Tèxtil d’Igualada-FITEX i de l’Agrupació Tèxtil FAGEPI. </p>
                                        <p>"Impulsor d’activitats de millora competitiva dins del sector tèxtil i moda, lidera projectes d’investigació, desenvolupament, innovació i internacionalització en el sector tèxtil-moda, amb més de 15 anys d’experiència."
                                        <div class="signature">
                                            <img src="<?= base_url() ?>img/about/david.png" alt="images">
                                        </div>
                                    </div>
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.about-charry -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>
            </li>

            <li>
                <section class="flat-row flat-about-charry pad-bottom125px pad-top100px bg-colorf7f7f7"> <a href="http://www.fitex.es"</a>
                    <div class="container">
                        <div class="row">
                            <div class="title-section">
                                <h4>La formació ho és tot</h4>
                                <h2><span>Els</span> Mentors</h2>
                            </div>
                            <div class="about-charry">
                                <div class="col-md-6">
                                    <div class="img-single">
                                        <img src="<?= base_url() ?>img/about/11111.png" alt="image">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="about-charry-post style1">
                                        <h4 class="title-post">
                                            <a href="http://www.fitex.es">Lídia Morcillo</a>
                                        </h4>
                                        <p>Responsable d’Innovació de la Fundació Privada per a la Innovació Tèxtil, FITEX.</p>
                                        <p>"Amb més de 20 anys d’experiència al sector, ha estat professora de cursos de Postgrau i ha realitzat diversos seminaris de transferència de coneixements en el sector moda i ha participat en diverses jornades, a reu del país, per parlar sobre innovacions en el món de la moda, moda sostenible i tendències de consum."
                                        </p>
                                        <div class="signature">
                                            <img src="<?= base_url() ?>img/about/lidia.png" alt="images">
                                        </div>
                                    </div>
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.about-charry -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>
            </li>

            <li>
                <section class="flat-row flat-about-charry pad-bottom125px pad-top100px bg-colorf7f7f7"> <a href="http://www.080barcelonashowroom.com"</a>
                    <div class="container">
                        <div class="row">
                            <div class="title-section">
                                <h4>La formació ho és tot</h4>
                                <h2><span>Els</span> Mentors</h2>
                            </div>
                            <div class="about-charry">
                                <div class="col-md-6">
                                    <div class="img-single">
                                        <img src="<?= base_url() ?>img/about/susana.png" alt="image">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="about-charry-post style1">
                                        <h4 class="title-post">
                                            <a href="http://www.fitex.es">Susana Barasoain</a>
                                        </h4>
                                        <p>Project Manager de MODACC (Clúster Català de la Moda) i de l’ Agrupación Española del Género de Punto.</p>
                                        <p>"Experta en internacionalització de l’empresa, ha desenvolupat  la seva carrera professional en contacte amb Showrooms, fires i relacions internacionals durant més de 15 anys, treballant a França i Regne Unit. Des de l'any 2011 desenvolupa la seva carrera professional a Barcelona i és, entre altres projectes, responsable de la participació de marques i dissenyadors en el 080 Barcelona Fashion Showroom."
                                        </p>
                                        <div class="signature">
                                            <img src="<?= base_url() ?>img/about/susanafirma.png" alt="images">
                                        </div>
                                    </div>
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.about-charry -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>
            </li>
            
            
        </ul>
    </div>
</div>

<!-- later new -->
<section class="flat-row flat-later-new pad-top50px" id="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="title-section">
                    <h4>Tot canvia</h4>
                    <h2><span>Darreres</span> Activitats</h2>
                </div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="later-new">
                    <?php if($blog->num_rows()>0): ?>
                        <div class="post">
                            <div class="img style1" style="background:url(<?= $blog->row(0)->foto ?>); background-size:cover;">
                                <a href="<?= $blog->row(0)->link ?>" style="">
                                    <img src="<?= $blog->row(0)->foto ?>" alt="image" style="visibility:hidden; height:266px; width:100%;"></a>
                                <span></span>
                            </div>

                            <div class="post-later-new">
                                <div class="title-later-new">
                                    <p><?= $blog->row(0)->categorias->row(0)->blog_categorias_nombre ?></p>
                                </div>                                
                                <h4 class="title-post">
                                    <a href="<?= $blog->row(0)->link ?>"><?= $blog->row(0)->titulo ?> </a>
                                </h4>
                                <div class="entry-content">
                                    <p style="height: 90px; overflow:hidden"><?= substr(strip_tags($blog->row(0)->texto),0,255) ?></p>
                                    <div class="more">
                                        <a href="<?= $blog->row(0)->link ?>">Llegir més</a>
                                    </div><!-- /more -->
                                </div> <!-- entry-content -->
                            </div><!-- /post-later-new -->      
                        </div> <!-- .post -->
                    <?php endif ?>
                    <?php if($blog->num_rows()>1): ?>    
                        <div class="post">
                            <div class="post-later-new">
                                <div class="title-later-new">
                                    <p><?= $blog->row(1)->categorias->row(0)->blog_categorias_nombre ?></p>
                                </div> 
                                <h4 class="title-post">
                                    <a href="<?= $blog->row(1)->link ?>"><?= $blog->row(1)->titulo ?></a>
                                </h4>
                                <div class="entry-content">
                                    <p style="height: 90px; overflow:hidden"><?= substr(strip_tags($blog->row(1)->texto),0,255) ?></p>
                                    <div class="more">
                                        <a href="<?= $blog->row(1)->link ?>">Llegir més</a>
                                    </div><!-- /more -->
                                </div> <!-- entry-content -->
                            </div><!-- /post-later-new --> 

                            <div class="img style2" style="background:url(<?= $blog->row(1)->foto ?>); background-size:cover;">
                                <a href="<?= $blog->row(1)->link ?>" style="">
                                    <img src="<?= $blog->row(1)->foto ?>" alt="image" style="visibility:hidden; height:266px; width:100%;"></a>
                                <span></span>
                            </div>

                        </div> <!-- .post -->
                    <?php endif ?>
                    <?php if($blog->num_rows()>2): ?> 
                        <div class="post">
                            <div class="img style1" style="background:url(<?= $blog->row(2)->foto ?>); background-size:cover;">
                                <a href="<?= $blog->row(2)->link ?>" style="">
                                    <img src="<?= $blog->row(2)->foto ?>" alt="image" style="visibility:hidden; height:266px; width:100%;"></a>
                                <span></span>
                            </div>

                            <div class="post-later-new">
                                <div class="title-later-new">
                                    <p><?= $blog->row(2)->categorias->row(0)->blog_categorias_nombre ?></p>
                                </div> 
                                <h4 class="title-post">
                                    <a href="<?= $blog->row(2)->link ?>"><?= $blog->row(2)->titulo ?></a>
                                </h4>
                                <div class="entry-content">
                                    <p style="height: 90px; overflow:hidden"><?= substr(strip_tags($blog->row(2)->texto),0,255) ?></p>
                                    <div class="more">
                                        <a href="<?= $blog->row(2)->link ?>">Llegir més</a>
                                    </div><!-- /more -->
                                </div> <!-- entry-content -->
                            </div><!-- /post-later-new -->      
                        </div> <!-- .post -->
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</section>
