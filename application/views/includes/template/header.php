<?php if(empty($scripts)): ?>
<div class="loader">
    <span class="loader1 block-loader"></span>
    <span class="loader2 block-loader"></span>
    <span class="loader3 block-loader"></span>
</div>
<?php endif ?>
<!-- Header --> 
<header id="header" class="header clearfix"> 
    <div class="header-wrap clearfix">
        <div id="logo" class="logo">
            <a href="<?= site_url() ?>" rel="home">
                <img src="<?= base_url() ?>img/logo.png" alt="image">
            </a>
        </div><!-- /.logo -->            
        <div class="nav-wrap">
            <div class="btn-menu">
                <span></span>
            </div><!-- //mobile menu button -->
            <nav id="mainnav" class="mainnav">
                <ul class="menu">                    
                    <li>
                        <a href="<?= site_url('p/futurmod') ?>">Què és?</a>
                        <ul class="submenu">
                            <li><a href="<?= site_url('p/pujar-curriculum') ?>">Participa</a></li>                            
                        </ul><!-- /.submenu -->
                        
                    </li> 
                    <li>
                        <a href="<?= site_url('projectes') ?>">Projectes</a>
                        <ul class="submenu">
                            <?php for($i = date("Y");$i>=2017;$i--): ?>
                                <li><a href="<?= site_url('projectes-any/'.$i) ?>">Projectes <?= $i ?></a></li>
                            <?php endfor ?>
                            <li><a href="<?= site_url('panel/index/add') ?>">Puja el teu projecte</a></li>                            
                            <li><a href="<?= site_url('panel') ?>">El teu projecte</a></li>                            
                        </ul><!-- /.submenu -->
                    </li>
                    <li id="seminariosnav">
                        <a href="<?= site_url('seminaris') ?>">Seminaris</a>
                        <ul class="submenu">
                            <?php                                 
                                $this->db->order_by('fecha','ASC');
                                $this->db->where('fecha >=',date("Y-m-d"));
                                $seminarios = $this->db->get_where('seminarios');
                                foreach($seminarios->result() as $s):
                            ?>
                            <li>
                                <a href="<?= site_url('seminaris/'.toUrl($s->id.'-'.$s->titulo)) ?>">
                                    <i class="fa fa-eye" aria-hidden="true" style="color:#ffd71b"></i> <?= strip_tags($s->titulo) ?>                                    
                                </a>
                            </li>
                            <?php endforeach ?>
                            <?php
                                $this->db->select('MIN(YEAR(fecha)) as anio');
                                $seminarios_viejos = $this->db->get('seminarios')->row()->anio;
                                $n = 0;
                                for($i=date("Y");$i>=$seminarios_viejos;$i--): $n++; if($n<3):
                            ?>
                                <li><a href="<?= site_url('seminaris-any/'.$i) ?>">Seminaris <?= $i ?></a></li>
                            <?php endif; endfor; ?>
                        </ul><!-- /.submenu -->
                    </li>
                    <li>
                        <a href="<?= site_url() ?>#mentors">Mentors</a>
                    </li>
                    <li>
                        <a href="<?= site_url() ?>#empresas">Empreses</a>
                    </li>
                   
                    <li>
                        <a href="<?= site_url('blog') ?>">Activitats</a>
                    </li>
                    <li>
                        <a href="<?= site_url('p/contacto') ?>">Contacte</a>
                    </li>
                    
                    <!--- Mobile -->
                    <?php if(empty($_SESSION['user'])): ?>
                        <li class="login visible-xs">
                            <a href="<?= base_url('panel') ?>">Entrar </a>
                        </li>  
                        <li class="signup visible-xs"> 
                            <a href="<?= base_url('panel') ?>">Registra't</a>
                        </li>
                    <?php else: ?>
                        <li class="login visible-xs">
                            <a href="<?= base_url('presentacions') ?>">Les meves Presentacions</a>
                        </li>  
                        <li class="signup visible-xs"> 
                            <a href="<?= base_url('main/unlog') ?>">Sortir</a>
                        </li>
                    <?php endif ?>   
                    
                </ul><!-- /.menu -->
            </nav><!-- /.mainnav -->    
        </div><!-- /.nav-wrap -->

        <div class="flat-information">            
            <ul class="flat-socials">
                <?php if(empty($_SESSION['user'])): ?>
                <li class="login">
                    <a href="<?= base_url('panel') ?>">Entrar </a>
                </li>  
                <li class="signup"> 
                    <a href="<?= base_url('panel') ?>">Registra't</a>
                </li>
                <?php else: ?>
                <li class="login">
                    <a href="<?= base_url('presentacions') ?>">Perfil 
                    <?php                     
                        if(!empty($_SESSION['user'])):
                            $seminarios = $this->db->get_where('presentaciones',array('user_id'=>$this->user->id));
                            if($seminarios->num_rows()>0):
                                ?>
                                    <span class="badge badge-info" id="presentacionscount"><?= $seminarios->num_rows() ?></span></a>
                                <?php
                            endif;
                        endif;                        
                     ?>
                </li>
                <li class="signup"> 
                    <a href="<?= base_url('main/unlog') ?>">Sortir</a>
                </li>
                <?php endif ?>                
                <li class="twitter">
                    <a href="https://twitter.com/search?q=%40futurmodinfo&src=typd">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                
            </ul>

            <div id ="s" class="search-box show-search">
                <a href="#search" class="flat-search"><i class="fa fa-search"></i></a> 
                <div class="submenu top-search">
                    <div class="widget widget_search">
                        <form class="search-form" method="get" action="http://www.google.com/search">
                            <input type="search" class="search-field" name="q" placeholder="Buscar …">
                            <input type="hidden" name="h1" placeholder="ca">
                            <input type="hidden" name="domains" value="<?= site_url() ?>">
                            <input type="hidden" name="sitesearch" value="<?= site_url() ?>">
                            <input type="submit" class="search-submit">
                        </form>
                    </div>
                </div>        
            </div><!-- /.show-search -->
        </div>
    </div><!-- /.header-inner --> 
</header><!-- /.header -->
