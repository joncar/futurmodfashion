<section class="page-section pt-90 pb-80 bg-main pattern relative">
        <div class="container">
          <div class="call-out-box clearfix with-icon">
            <div class="row call-out-wrap">
              <div class="col-md-5">
                <h6 class="title-section-top gray font-4">Subscríbete hoy</h6>
                <h2 class="title-section alt-2"><span>Coje</span> las últimas ofertas</h2>
                <i class="flaticon-suntour-email call-out-icon"></i>
              </div>
              <div class="col-md-7">
                <form action="" method="post" class="form contact-form mt-10" onsubmit="return subscribir(this)">
                  <div class="input-container">
                    <input id="email" placeholder="Escribe tú email" value="" name="email" class="newsletter-field mb-0 form-row" type="text"><i class="flaticon-suntour-email icon-left"></i>
                    <button type="submit" class="subscribe-submit">
                                    <i class="flaticon-suntour-arrow icon-right"></i>
                                </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section><!-- ! call out section-->
<script>
function subscribir(form){
  $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:$("#email").val()},function(data){
      emergente(data);
  });
  return false;
}
</script>