<!-- Javascript -->        
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.easing.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/owl.carousel.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.flexslider.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.countdown.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.sticky.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/parallax.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/imagesloaded.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery-countTo.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery-waypoints.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery-validate.js"></script>
    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&language=es"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/gmap3.min.js"></script>
    
    
    <script type="text/javascript" src="<?= base_url() ?>js/template/main.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/slider.js"></script>
    <script>    
    $(document).on('click','.header a',function(e){
        var el = $(this);
        clickDownScroll(e,el);
    });
    $(document).on('touchstart','.header a',function(e){
        var el = $(this);
        clickDownScroll(e,el);
    });
    
    function clickDownScroll(e,el){        
        var target = el.attr('href');
        target = target.split('#');
        if(target.length===2){
            if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {           
                        window.scrollTo($("#"+target[1]).offset().left,$("#"+target[1]).offset().top); // first value for left offset, second value for top offset
            }else{
                $("html, body").stop().animate({scrollTop:$("#"+target[1]).offset().top},1500, 'swing');     
            }
        }
        document.location.href=el.attr('href');        
    }
    
    function sumarCorazon(id){
        $.post('<?= base_url('proyectos/frontend/sumarCorazon') ?>/'+id,{},function(data){
            $('#likes'+id).html('<i class="icon-heart"></i> '+data);
        });
    }
</script>
<script>
function subscribir(form){
  var email = $(form).find('input[type="email"]').val();
  $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:email},function(data){
      $("#subscralert,#footsubscralert").html(data);
  });
  return false;
}
</script>

<script>
    function remoteConnection(url,data,callback){        
        return $.ajax({
            url: '<?= base_url() ?>'+url,
            data: data,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:callback
        });
    };

    function insertar(url,datos,callback,callbackError){
        $("#result").removeClass('alert alert-danger').html('');
        $("button[type=submit]").attr('disabled',true);
        var uri = url.replace('insert','insert_validation');
        uri = uri.replace('update','update_validation');
        remoteConnection(uri,datos,function(data){
            $("button[type=submit]").attr('disabled',false);
            data = $(data).text();
            data = JSON.parse(data);
            
            if(data.success){
              remoteConnection(url,datos,function(data){
                data = $(data).text();
                data = JSON.parse(data);
                callback(data);
              });
            }else{
              $("#result").addClass('alert alert-danger').html(data.error_message);
              callbackError(data);
            }
        });
    }
</script>
<?php $this->load->view('predesign/gallery') ?>
<?php $this->load->view('predesign/barracookies'); ?>
