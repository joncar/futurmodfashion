<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function ajustes(){            
            $crud = $this->crud_function('',''); 
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
