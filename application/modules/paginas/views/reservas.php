<div class="page-title parallax parallax4" style=" background-repeat: no-repeat; background-position: 50% 0px; background-image: url(../../img/parallax/bg-parallax4_7.jpg);  background-size: inherit;" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Reservas</h2>
                </div><!-- /.page-title-heading -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><a href="<?= site_url() ?>">Home</a></li>
                        <li>Reservas</li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title parallax -->

<section class="flat-row portfolio-row-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="contact-widget">
                    <div class="widget contact-info">
                        <h4 class="widget-title"> FORMULARI A OMPLIR JORNADA 2 DE FEBRER</h4>                        
                    </div>
                </div>
            </div><!-- /.col-md-4 -->
            <div class="col-md-8">
                <?= 
                   !empty($_SESSION['msj'])?$_SESSION['msj']:''
                ?>
                <div class="widget-contactform">
                    <form method="post" action="<?= base_url('reservas/frontend/reservas/insert') ?>?redirect=<?= base_url('p/invitacion') ?>">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: right; margin: 8px;">
                                * obligatori
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?= form_dropdown('empresa',array('Empresa Consolidada'=>'Empresa Consolidada','Emprenedor'=>'Emprendedor/a'),'','class="form-control" style="height:50px; padding:13px;"') ?></p>
                                <p><input id="nombre" name="nombre" type="text" value="" placeholder="Nom i cognoms assistent *" required="required"></p>
                                <p><input id="carrec" name="carrec" type="text" value="" placeholder="Càrrec *" required="required"></p>
                                <p><input id="telefono" name="telefono" type="text" value="" placeholder="Tel. De contacte *" required="required"></p>
                                <p><input id="direccion" name="direccion" type="text" value="" placeholder="Adreça *" required="required"></p>
                                <p><input id="cp" name="cp" type="text" value="" placeholder="CP i Població *" required="required"></p>
                            </div><!-- /.col-md-6 -->
                            <div class="col-md-6">
                                <p><input id="entidad" name="entidad" type="text" value="" placeholder="Empresa/entitat *" required="required"></p>
                                <p><input id="nif_empresa" name="nif_empresa" type="text" value="" placeholder="NIF Empresa"></p>
                                <p><input id="cp_empresa" name="cp_empresa" type="text" value="" placeholder="CP i Població Empresa *" required="required"></p>
                                <p><input id="email" name="email" type="email" value="" placeholder="Mail de contacte *" required="required"></p>
                                <p><input id="web" name="web" type="text" value="" placeholder="Web empresa"></p>
                                <span class="form-submit">                                                                         
                                    <input name="submit" type="submit" id="submit" class="submit" value="Enviar Email">
                                </span>
                            </div><!-- /.col-md-6 -->
                        </div><!-- /.row -->
                    </form>
                </div><!-- /.col-md-8 -->
            </div><!-- /.widget-contactform -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /flat-row portfolio-row-page -->
<?php $_SESSION['msj'] = null ?>
