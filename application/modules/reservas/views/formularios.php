<div class="page-title parallax parallax4" style=" background-repeat: no-repeat; background-position: 50% 0px; background-image: url(../../img/parallax/bg-parallax4_7.jpg);  background-size: inherit;" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Reservas</h2>
                </div><!-- /.page-title-heading -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><a href="<?= site_url() ?>">Home</a></li>
                        <li>Reservas</li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title parallax -->

<section class="flat-row portfolio-row-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="contact-widget">
                    <div class="widget contact-info">
                        <h4 class="widget-title"> <?= mb_strtoupper($formulario->titulo) ?></h4>                        
                    </div>
                </div>
            </div><!-- /.col-md-4 -->
            <div class="col-md-8">
                <div id="result"></div>
                <div class="widget-contactform">
                    
                    <?php if($formulario->plazas>0 && $this->db->get_where('reservas',array('formularios_id'=>$id,'status'=>3))->num_rows() >= $formulario->plazas): ?>
                    	Lo sentimos pero se han agotado las plazas para asistir al evento
                    <?php elseIf(strtotime(date("Y-m-d"))>strtotime($formulario->fecha_cierre)): ?>
                    	Lo sentimos pero se ha finalizado el tiempo de inscripción para este evento
                    <?php else: ?>
                    <form method="post" onsubmit="return inscribirse(this)" action="">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: right; margin: 8px;">
                                * obligatori
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?= form_dropdown('empresa',array('Empresa Consolidada'=>'Empresa Consolidada','Emprenedor'=>'Emprendedor/a'),'','class="form-control" style="height:50px; padding:13px;"') ?></p>
                                <p><input id="nombre" name="nombre" type="text" value="" placeholder="Nom i cognoms assistent *"></p>
                                <p><input id="carrec" name="carrec" type="text" value="" placeholder="Càrrec *"></p>
                                <p><input id="telefono" name="telefono" type="text" value="" placeholder="Tel. De contacte *"></p>
                                <p><input id="direccion" name="direccion" type="text" value="" placeholder="Adreça *"></p>
                                <p><input id="cp" name="cp" type="text" value="" placeholder="CP i Població *"></p>
                            </div><!-- /.col-md-6 -->
                            <div class="col-md-6">
                                <p><input id="entidad" name="entidad" type="text" value="" placeholder="Empresa/entitat *"></p>
                                <p><input id="nif_empresa" name="nif_empresa" type="text" value="" placeholder="NIF Empresa"></p>
                                <p><input id="cp_empresa" name="cp_empresa" type="text" value="" placeholder="CP i Població Empresa *"></p>
                                <p><input id="email" name="email" type="email" value="" placeholder="Mail de contacte *"></p>
                                <p><input id="web" name="web" type="text" value="" placeholder="Web empresa"></p>
                                <span class="form-submit">  
                                	<input type="hidden" name="formularios_id" value="<?= $id ?>">                                                                       
                                    <input name="submit" type="submit" id="submit" class="submit" value="Enviar invitación">
                                </span>
                            </div><!-- /.col-md-6 -->
                        </div><!-- /.row -->
                    </form>
					<?php endif ?>

                </div><!-- /.col-md-8 -->
            </div><!-- /.widget-contactform -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /flat-row portfolio-row-page -->
<?php $_SESSION['msj'] = null ?>
<script>
	function inscribirse(f){
		var f = new FormData(f);
        $("#submit").val('Enviando información');
        $("#submit").attr('disabled',true);
		insertar('reservas/frontend/reservas/insert',f,function(data){
			$("#result").addClass('alert alert-success').html('La seva invitació s\'ha enviat amb èxit');
            $("#submit").val('Enviar invitación');
            $("#submit").attr('disabled',false);
		},function(){            
            $("#submit").val('Enviar invitación');
            $("#submit").attr('disabled',false);
        });
		return false;
	}
</script>