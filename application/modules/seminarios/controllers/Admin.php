<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function seminarioss(){
            $this->as['seminarioss'] = 'seminarios';
            $crud = $this->crud_function('','');
            $crud->set_subject('Seminarios');
            $crud->set_field_upload('foto','img/seminarios');
            $crud->set_field_upload('presentacion','files');
            $crud->set_field_upload('presentacion2','files');
            $crud->set_relation('formulario','formularios','titulo');
            $crud->display_as("categorias_seminarios_id","Categoria")
                 ->display_as('presentacion','Programa')
                 ->display_as('presentacion2','Presentación');
            $crud->field_type('tags','tags')
                 ->field_type('status','dropdown',array("1"=>"Publicado"));
            $crud->field_type('user','string',$this->user->nombre.' '.$this->user->apellido);
            $crud->columns('categorias_seminarios_id','foto','titulo','fecha','hora','tags','user','visitas','ubicacion','anio');
            $crud->set_clone();
            $crud = $crud->render();
            $crud->title = 'Seminarios';
            $this->loadView($crud);
        }
        
        public function categorias_seminarios(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Categoria');
            $crud = $crud->render();
            $crud->title = 'Categorias';
            $this->loadView($crud);
        }
    }
?>
