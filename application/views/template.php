<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title><?= empty($title)?'Futurmod Fashion':$title ?></title>
    <meta name="description" content="FUTUR MOD és un projecte orientat a donar suport a emprenedors i micropimes de nova creació en el sector de la moda, per facilitar la seva consolidació i creixement. Adreça
Avda. Mestre Montaner, 86. 08700 Igualada, BARCELONA. Tel. 938 03 57 62. mail:info@futurmod.fashion">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/bootstrap.css" >        
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/revolution-slider.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/colors/color1.css" id="colors">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/animate.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/fonts/fi/flaticon.css">
    <link rel="shortcut" href="<?= base_url() ?>img/favicon.png">
    <link rel="apple-touch-icon" href="<?= base_url() ?>img/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>img/favicon.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>img/favicon.png">
    <link href="icon/favicon.png" rel="shortcut icon">   
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/bootstrap.min.js"></script>
    <script src='//www.google.com/recaptcha/api.js'></script>
     <?php if(!empty($myscripts)) echo $myscripts ?>
</head> 
<body class="header-sticky home-boxed">
    <?= $this->load->view('includes/template/header'); ?>
    <?= $this->load->view($view); ?>
    <?= $this->load->view('includes/template/footer'); ?>
    <?php if(empty($scripts)){$this->load->view('includes/template/scripts');} ?>      
</body>
</html>
