<footer class="footer padding-top120px">
    <div class="footer-widgets">
        <div class="container">
            <div class="row"> 
                <div class="col-md-5">  
                    <div class="widget widget-text">
                        <h4 class="widget-title">Què és Futur mod?</h4>
                        <div class="text">                                
                            <p>FUTUR MOD és un projecte orientat a donar suport a emprenedors i micropimes de nova creació en el sector de la moda, per facilitar la seva consolidació i creixement.</p>
                        </div><!-- /.textwidget -->                        
                        <ul class="flat-socials">                            
                            <li class="twitter">
                                <a href="https://twitter.com/search?q=%40futurmodinfo&src=typd">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                   
                        </ul>
                    </div><!-- /.widget -->      
                </div><!-- /.col-md-5 --> 

                <div class="col-md-3">  
                    <div class="widget contact-info">
                        <h4 class="widget-title">Adreça</h4>
                        <ul>
                            <li class="address"><a href="#">Avda. Mestre Montaner, 86</a></li>
                            <li class="address1"><a href="#">08700 Igualada, BARCELONA</a></li>
                            <li class="phone"><a href="tel:938035762"> 938 03 57 62</a></li>
                            <li class="email"><a href="mailto:info@futurmod.fashion">info@futurmod.fashion</a></li>
                        </ul>
                    </div>  
                </div><!-- /.col-md-3 -->

                <div class="col-md-4">  
                    <div class="widget widget_mc4wp">
                        <div id="mc4wp-form-1" class="form mc4wp-form clearfix">
                            <h4 class="widget-title">Subscriu-te</h4>
                            <div class="mail-chimp">
                                <p style=" margin-bottom: 20px; ">Si vols estar al dia de totes les noticies del sector, subscriu-te al butlletí de noticies que enviem regularment.</p>
                                <div id="footsubscralert"></div>
                                <form action="#"   onsubmit="return subscribir(this)" id="mailform" method="get">
                                    <input type="email" id="m" class="mmm" placeholder="El teu Email">
                                    <input type="submit" value="" id="gmail">
                                </form>
                            </div>
                        </div>
                    </div>     
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->
            <!--- Logos --->
            <div id="footermsgespecial" class="row">
                <div>Aquest projecte és una iniciativa de:</div>
                <img src="<?= base_url() ?>img/logos.png" >
                <div>Amb el suport de:</div>
                <img src="<?= base_url() ?>img/logos2.png" >
                <div>Amb la col.laboració de:</div>
                <img src="<?= base_url() ?>img/logos3.png" >
                <div>Aquest projecte està subvencionat pel Servei Públic d’Ocupació de Catalunya en el marc del Programa de projectes innovadors i experimentals.
                </div>
            </div>
            <!-- /Logos ---->
        </div><!-- /.container -->
    </div><!-- /.footer-widgets -->
</footer><!-- /footer -->

<div class="bottom">        
    <div class="container-bottom">
        <div class="copyright"> 
            <p>Tots els drets reservats 2017 © Hipo</p>
        </div>                

        <ul class="text-right">

            <li><a href="p/avis.html">Avís Legal</a></li>
        </ul>              
    </div><!-- /.container-bottom -->            
</div><!-- /bottom -->

<!-- Go Top -->
<a class="go-top">
    <i class="fa fa-chevron-up"></i>
</a>
