<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function proyectoss(){
            $this->as['proyectoss'] = 'proyectos';
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Proyectos');
            $crud->set_field_upload('foto','img/proyectos');
            $crud->display_as("categorias_proyectos_id","Categoria")
                 ->display_as('empresa','Nombre de la empresa')
                 ->display_as('descripcion_corta','Descripcion Corta (100 caracteres max).')
                 ->field_type('status','true_false',array("0"=>"Borrador",'1'=>"Publicado"))
                 ->field_type('likes','hidden','[]');
            $crud->display_as('descripcion_inicial','Descripció pública del projecte')
                 ->display_as('texto','Descripció privada del projecte')
                 ->display_as('je8701ad4.nombre','Usuario')
                 ->display_as('je8701ad4.email','Email');
            $crud->columns('id','foto','categorias_proyectos_id','titulo','empresa','je8701ad4.nombre','je8701ad4.email','visitas','status','Enlace Privado','anio');
            $crud->set_relation('user_id','user','{nombre}|{email}');
            $crud->callback_column('je8701ad4.nombre',function($val,$row){return explode('|',$row->se8701ad4)[0];});
            $crud->callback_column('je8701ad4.email',function($val,$row){return explode('|',$row->se8701ad4)[1];});
            $crud->callback_column('Enlace Privado',function($val,$row){return '<a href="'.site_url('projectes/'.$row->id.'/'.'s2w'.substr(md5($row->id),0,8)).'">Enlace Privado</a>';});
            $crud->add_action('<i class="fa fa-image"></i> Subir Fotos','',base_url('proyectos/admin/fotos').'/');
            $crud->field_type('tags','tags');
            $crud = $crud->render();
            $crud->title = 'Proyectos';
            $this->loadView($crud);
        }
        
        public function categorias_proyectos(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Categorias');
            $crud = $crud->render();
            $crud->title = 'Categorias';
            $this->loadView($crud);
        }
        
        public function fotos_usuarios($proyecto = '')
        {
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('proyectos_fotos')
                 ->set_relation_field('proyectos_id')
                 ->set_url_field('foto')
                 ->set_ordering_field('orden')
                 ->set_image_path('img/proyectos');
            $crud->module = 'proyectos';
            $crud = $crud->render();
            $crud->output= $this->load->view('frontend/fotos_usuarios',array('proyecto'=>$proyecto,'output'=>$crud->output),TRUE);
            $crud->myscripts = get_header_crud($crud->css_files,$crud->js_files,TRUE);
            $this->loadView($crud);
        }
        
        public function fotos()
        {
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('proyectos_fotos')
                 ->set_relation_field('proyectos_id')
                 ->set_url_field('foto')
                 ->set_ordering_field('orden')
                 ->set_image_path('img/proyectos');
            $crud->module = 'proyectos';
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
