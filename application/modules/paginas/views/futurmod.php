<div class="page-title parallax parallax4" style=" background-position: 50% 97px; background-image: url(../../img/parallax/bg-parallax4_4.jpg);  background-size: inherit;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">FuturMod</h2>
                </div><!-- /.page-title-heading -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><a href="#">Home</a></li>
                        <li>Què és?</li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->
</div><!-- /page-title parallax -->

<section class="flat-row flat-about-charry">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="title-section">
                    <h4>tot el que has saber de</h4>
                    <h2><span>Futur</span> Mod</h2>
                </div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
        <div class="about-charry">
            <div class="row">
                <div class="col-md-6">
                    <img src="<?= base_url() ?>img/about/1.jpg" alt="image">
                </div>
                <div class="col-md-6">
                    <div class="about-charry-post">
                        <h4 class="title-post">
                            <a href="#">Impulsant el negocis de Moda del Futur</a>
                        </h4>
                        <p class="style-v1">FUTUR MOD es un projecte orientat a donar suport a emprenedors i micropimes de nova creació en el sector de la moda, per facilitar la seva consolidació i creixement.
                        </p>
                        <p>FUTURMOD pretén establir col·laboracions estables entre emprenedors que comencen el seu negoci i empreses consolidades del sector de la moda per tal de contribuir a millorar la seva competitivitat i aportar beneficis a ambdues parts.
                        </p>
                        <p>El projecte pretén també construir noves cadenes de valor i xarxes de relació entre joves emprenedors, empreses consolidades, administracions, escoles de disseny i centres de recerca per impulsar la innovació en el sector de la moda.
                        </p>
                        <div class="about-button">
                            <button class="flat-button button-style" onclick="document.location.href='<?= site_url('p/contacto') ?>'">Contacte</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container -->   
</section><!-- /flat-row flat-about-charry -->

<section class="flat-row icon-left-square">
    <div class="container">
        <div class=" flat-icon">
            <div class="row">
                <div class="col-md-4">
                    <div class="icon">
                        <div class="icon-square">
                            <i class="icon-people"></i>
                        </div>
                        <div class="icon-post">
                            <h6 class="title-post">
                                <a href="#">Acollida</a>
                            </h6>
                            <p>FUTURMOD comença amb la recepció de la teva idea i/o projecte empresarial. Volem conèixer la teva proposta de valor i dissenyar un itinerari per acompanyar el teu projecte empresarial.
                            </p>
                            <div class="more">
                                <a href="<?= site_url('p/pujar-curriculum') ?>">Pujar CV</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon">
                        <div class="icon-square">
                            <i class="icon-graduation"></i>
                        </div>
                        <div class="icon-post">
                            <h6><a href="#">Mentoring</a></h6>
                            <p>Experts del negoci de la moda t’ajuden a impulsar el creixement del teu projecte empresarial. Podràs triar entre diferents col·laboradors de FUTURMOD per trobar el professional més adient a la teva activitat i amb qui puguis establir una relació de confiança.
                            </p>
                            <div class="more">
                                <a href="#mentors">Persones</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon">
                        <div class="icon-square">
                            <i class="icon-wrench"></i>
                        </div>
                        <div class="icon-post">
                            <h6><a href="#">Seminaris</a></h6>
                            <p>Podràs participar en diferents tallers i seminaris sobre operacions de producció, finances, màrqueting, identitat de marca, comerç internacional, aspectes jurídics tots enfocats a facilitar el creixement i consolidació de negocis de moda.
                            </p>
                            <div class="more">
                                <a href="<?= site_url('seminaris') ?>">Veure Tots</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /flat-row icon-left-square -->

<!-- Flat event -->
<section class="flat-row pad-top50px" id="seminaris">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="title-section">
                    <h4>La formació al capdavant  </h4>
                    <h2><span></span> Seminaris</h2>
                </div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
        <div class="row">
            <?php foreach($seminarios->result() as $p): ?>
                <div class="col-md-4 col-sm-6">
                    <div class="flat-event">
                        <div class="event-img">
                            <img src="<?= $p->foto ?>" alt="images">
                        </div>
                        <div class="event-content">
                            <div class="flat-time">
                                <span class="day"><?= date("d",strtotime($p->fecha)) ?></span>
                                <span class="month"><?= strftime("%b",strtotime($p->fecha)) ?></span>
                                <span class="hours"><?= $p->hora ?></span>
                            </div>
                            <div class="event-title">
                                <h4 class="title"><a href="<?= $p->link ?>"><?= $p->titulo ?></a></h4>
                                <p class="add"><i class="fa fa-map-marker"></i><?= $p->ubicacion ?></p>
                            </div>
                        </div>
                    </div>
                </div><!-- /col-md-4 -->
            <?php endforeach ?>
        </div><!-- /row -->
        <div class="flat-divider d50px"></div>
        <div class="row">
            <div class="button-center">
                <button class="flat-button button-style" onclick="document.location.href='<?= site_url('seminaris') ?>'">Veure Més</button>
                <a href="<?= site_url('seminaris') ?>" class="hidden">Veure Més</a>
            </div>
        </div>
    </div><!-- /container -->
</section><!-- /flat-row flat-icon icon-left -->

<section class="flat-row flat-member" id='mentors'>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="title-section">
                    <h4>Treballem per a vosaltres</h4>
                    <h2><span>Els </span> Mentors</h2>
                </div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="flat-member-carousel" data-item="3" data-nav="true" data-dots="false" data-auto="false">
                <div class="cs-module-4">
                    <div class="item">
                        <div class="overlay"></div>
                        <img src="<?= base_url() ?>img/member/1.jpg" alt="image">
                    </div>
                    <div class="demo">
                        <div class="cs-post">
                            <div class="cs-post-header">
                                <div class="cs-category-links">
                                    <h6 class="name"><a href="#">Llorenç Palomas</a></h6>
                                    <span class="position">Expert en e-comerç</span>

                                    <div class="cs-post-footer">
                                        <div class="cs-footer-share">
                                            <a href="https://es.linkedin.com/in/llorencpalomas" target="_blank"><i class="fa fa-linkedin"></i></a>
                                            <a href="https://twitter.com/LlorensP" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="https://www.esdemarketing.com/" target="_blank"><i class="fa fa-rss"></i></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div> 
                        </div> 
                    </div>
                </div>

                <div class="cs-module-4">
                    <div class="item">
                        <div class="overlay"></div>
                        <img src="<?= base_url() ?>img/member/2.jpg" alt="image">
                    </div>
                    <div class="demo">
                        <div class="cs-post">
                            <div class="cs-post-header">
                                <div class="cs-category-links">
                                    <h6 class="name"><a href="#">Albert Pallarès</a></h6>
                                    <span class="position">Consultor per a PIMES
</span>

                                    <div class="cs-post-footer">
                                        <div class="cs-footer-share">
                                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div> 
                        </div> 
                    </div>
                </div>

                <div class="cs-module-4">
                    <div class="item">
                        <div class="overlay"></div>
                        <img src="<?= base_url() ?>img/member/3.jpg" alt="image">
                    </div>
                    <div class="demo">
                        <div class="cs-post">
                            <div class="cs-post-header">
                                <div class="cs-category-links">
                                    <h6 class="name"><a href="#">Markus Förster</a></h6>
                                    <span class="position">Enginyer industrial</span>

                                    <div class="cs-post-footer">
                                        <div class="cs-footer-share">
                                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div> 
                        </div> 
                    </div>
                </div>

                <div class="cs-module-4">
                    <div class="item">
                        <div class="overlay"></div>
                        <img src="<?= base_url() ?>img/member/4.jpg" alt="image">
                    </div>
                    <div class="demo">
                        <div class="cs-post">
                            <div class="cs-post-header">
                                <div class="cs-category-links">
                                    <h6 class="name"><a href="#">David Garcia</a></h6>
                                    <span class="position">Impulsor d’activitats</span>

                                    <div class="cs-post-footer">
                                        <div class="cs-footer-share">
                                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div> 
                        </div> 
                    </div>
                </div>

                <div class="cs-module-4">
                    <div class="item">
                        <div class="overlay"></div>
                        <img src="<?= base_url() ?>img/member/5.jpg" alt="image">
                    </div>
                    <div class="demo">
                        <div class="cs-post">
                            <div class="cs-post-header">
                                <div class="cs-category-links">
                                    <h6 class="name"><a href="#">Lídia Morcillo</a></h6>
                                    <span class="position">Responsable Innovac. Fitex</span>

                                    <div class="cs-post-footer">
                                        <div class="cs-footer-share">
                                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div> 
                        </div> 
                    </div>
                </div>

                <div class="cs-module-4">
                    <div class="item">
                        <div class="overlay"></div>
                        <img src="<?= base_url() ?>img/member/susana.jpg" alt="image">
                    </div>
                    <div class="demo">
                        <div class="cs-post">
                            <div class="cs-post-header">
                                <div class="cs-category-links">
                                    <h6 class="name"><a href="#">Susana Barasoain</a></h6>
                                    <span class="position">Project Manager de MODACC</span>

                                    <div class="cs-post-footer">
                                        <div class="cs-footer-share">
                                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                            <a href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
                </div>
            </div><!-- /flat-member-carousel -->
        </div><!-- /row -->
    </div><!-- /container -->
</section><!-- /flat-row flat-member -->

<section class="flat-row flat-support-us no-padding-bottom" style="padding: 5px 0;    padding-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="flat-support">
                    <div class="support-wrapper">
                        <h4 class="title-post">
                            <a href="#">Què oferim?</a>
                        </h4>
                        <p>FUTURMOD posa a la teva disposició un servei de Mentoring que t’ajudarà en la definició de la teva estratègia empresarial, en el desenvolupament de les àrees clau del teu negoci i et facilitarà contactes que t’ajudaran a créixer.
                            Els Mentors són persones amb una destacada trajectòria professional en el sector de la moda i experts en una àrea concreta; estratègia, màrqueting, venda internacional, finances, producció i ecommerce entre d’altres.
                            Podràs seleccionar el teu Mentor d’una relació de col·laboradors vinculats a la iniciativa FUTURMOD.
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="support-icon">
                                <div class="icon">
                                    <i class="icon-like"></i>
                                    <h6 class="title-post">
                                        <a href="#">Recepció</a>
                                    </h6><!-- /title-post -->
                                </div><!-- /icon -->
                                <div class="icon-post-v1">
                                    <p>Donem suport a nous projectes empresarials en el sector de la Moda. Prioritzem projectes amb menys de 5 anys de vida i que fonamentin la seva proposta de valor en el disseny de nous productes, l’economia circular i la digitalització, aplicats al sector de la moda. </p>
                                </div><!-- /icon-post-v1 -->
                            </div><!-- /support-icon -->
                        </div><!-- /col-md-6 -->

                        <div class="col-md-6">
                            <div class="support-icon">
                                <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                    <h6 class="title-post">
                                        <a href="#">Anàlisi</a>
                                    </h6><!-- /title-post -->
                                </div><!-- /icon -->
                                <div class="icon-post-v1">
                                    <p>Analitzem la teva idea i/o projecte empresarial  amb la finalitat de dissenyar un itinerari de creixement. Et proposem un itinerari formatiu, l’assignació d’un mentor expert sectorial que acompanyi el teu projecte i contactes amb empreses consolidades del sector – marques, productors, distribuïdors- que puguin ajudar a desenvolupar la teva idea o projecte empresarial.
                                        d</p>
                                </div><!-- /icon-post-v1 -->
                            </div><!-- /support-icon -->
                        </div><!-- /col-md-6 -->
                    </div><!-- /row -->

                    <div class="row">
                        <div class="flat-divider d30px"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="support-icon">
                                <div class="icon">
                                    <i class="icon-compass"></i>
                                    <h6 class="title-post">
                                        <a href="#">Maching</a>
                                    </h6><!-- /title-post -->
                                </div><!-- /icon -->
                                <div class="icon-post-v1">
                                    <p>Treballem per connectar l’emprenedor o “start up” del sector de la moda amb empreses consolidades en aquest sector per tal de facilitar relacions mercantils i/o d’inversió.  Connectem talent emprenedor, experiència i recursos per fer créixer nous negocis en el sector de la moda.
                                    </p>
                                </div><!-- /icon-post-v1 -->
                            </div><!-- /support-icon -->
                        </div><!-- /col-md-6 -->

                        <div class="col-md-6">
                            <div class="support-icon">
                                <div class="icon">
                                    <i class="icon-eyeglass"></i>
                                    <h6 class="title-post">
                                        <a href="#">Seguiment</a>
                                    </h6><!-- /title-post -->
                                </div><!-- /icon -->
                                <div class="icon-post-v1">
                                    <p>Acompanyem el desenvolupament del teu projecte empresarial durant un període de 6 mesos i monitoritzem les relacions establertes amb empreses consolidades per tal de valorar l’efecte d’acceleració del projecte sobre el teu negoci i la capacitat d’innovació aportat a l’empresa consolidada.
                                    </p>
                                </div><!-- /icon-post-v1 -->
                            </div><!-- /support-icon -->
                        </div><!-- /col-md-6 -->
                    </div><!-- /row -->
                </div><!-- /support-teaser -->
            </div><!-- /col-md-6 -->

            <div class="col-md-6">
                <div class="container-fluid">
                    <div class="img-wrapper">
                        <img src="<?= base_url() ?>img/member/6.jpg" alt="image">
                    </div>
                </div>
            </div><!-- /col-md-6 -->
        </div><!-- /row -->
    </div><!-- /container -->
</section><!-- /flat-row flat-support-us no-padding-bottom -->




<div class="page-title parallax parallax9">
    <div class="overlay"></div>            
    <div class="container">
        <div class="row">
            <div class="col-md-12">              
                <div class="page-title-heading">  
                    <h2 class="title white">Dóna a conèixer, <span>el teu projecte </span> <br>fes que tothom el conegi!</h2>
                </div><!-- /.page-title-heading -->
                <div class="parallax-button style2">
                    <button class="flat-button button-style" onclick="document.location.href='<?= site_url('panel') ?>'">Puja'l Aquí</button>
                </div>
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->          
</div><!-- /page-title parallax parallax6 -->

<section class=" flat-row partners-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="title-section">
                    <h4>TREBALLEM PÉL FUTUR</h4>
                    <h2><span>Empreses</span> Consolidades</h2>
                </div>
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-10 col-lg-offset-1 clearfix">
                <div class="partners">
                    <div class="partner top">
                        <img src="<?= base_url() ?>img/clients/1.jpg" alt="image">
                    </div>
                    <div class="partner top">
                        <img src="<?= base_url() ?>img/clients/2.jpg" alt="image">
                    </div>
                    <div class="partner top">
                        <img src="<?= base_url() ?>img/clients/3.jpg" alt="image">
                    </div>
                    <div class="partner top last">
                        <img src="<?= base_url() ?>img/clients/4.jpg" alt="image">
                    </div>
                    <div class="partner bottom">
                        <img src="<?= base_url() ?>img/clients/5.jpg" alt="image">
                    </div>
                    <div class="partner bottom">
                        <img src="<?= base_url() ?>img/clients/6.jpg" alt="image">
                    </div>
                    <div class="partner bottom">
                        <img src="<?= base_url() ?>img/clients/7.jpg" alt="image">
                    </div>
                    <div class="partner bottom last">
                        <img src="<?= base_url() ?>img/clients/8.jpg" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="page-title parallax parallax2">
    <div class="overlay v1"></div>            
    <div class="container">
        <div class="row">
            <div class="col-md-3">                    
                <div class="flat-counter">
                    <div class="numb-count" data-to="324" data-speed="2000" data-waypoint-active="yes">324</div>
                    <div class="name-count"><p>EMPRENEDORS</p></div>
                </div><!-- /flat-counter -->
            </div><!-- /.col-md-3 -->  

            <div class="col-md-3">                    
                <div class="flat-counter">
                    <div class="numb-count" data-to="984" data-speed="2000" data-waypoint-active="yes">984</div>
                    <div class="name-count"><p>EMPRESES</p></div>
                </div><!-- /.roll-counter -->
            </div><!-- /.col-md-3 -->

            <div class="col-md-3">                    
                <div class="flat-counter counter-donation">
                    <div class="numb-count" data-to="232" data-speed="2000" data-waypoint-active="yes">232</div>
                    <div class="name-count"><p>MENTORS</p></div>
                </div><!-- /flat-counter -->
            </div><!-- /.col-md-3 -->

            <div class="col-md-3">                    
                <div class="flat-counter">
                    <div class="numb-count" data-to="24" data-speed="2000" data-waypoint-active="yes">24</div>
                    <div class="name-count"><p>SEMINARIS</p></div>
                </div><!-- /flat-counter -->
            </div><!-- /.col-md-3 -->
        </div><!-- /.row -->  
    </div><!-- /.container -->          
</div><!-- /page-title parallax parallax2 -->

