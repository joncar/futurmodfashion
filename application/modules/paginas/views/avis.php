<div class="page-title parallax parallax4" style=" background-position: 50% 49px; background-image: url(../../img/parallax/bg-parallax4_7.jpg);  background-size: inherit;" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Avís Legal</h2>
                </div><!-- /.page-title-heading -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><a href="#">Home</a></li>
                        <li>Avís Legal</li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title parallax -->

<section class="flat-row portfolio-row-page">
    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <div class="contact-widget">
                    <div class="widget contact-info">
                        <h4 class="widget-title">Informació general</h4>
                        <ul>
                            <div class="contenidor_contingut_sense_espai"><table class="taula_continguts" summary="Continguts"><tbody><tr><td class="td_justificat">
                                            <p>
                                                <strong><strong>Titularitat de la pàgina web.</strong>
                                                </strong>
                                                El nom del domini www.futurmod.fashion, està enregistrat a favor de Agrupació Tèxtil Fagepi, amb CIF G08604217 i domicili social en Avinguda Mestre Muntaner, 86, 08700-Igualada (Barcelona). Telèfon de contacte +34 938032993 / +34 938035762 i adreça de correu electrònic <a href="mailto:central@fagepi.net" target="_blank">central@fagepi.net</a>

                                            <hr>
                                            <p class="titol_nivell_1">
                                                <strong><strong>Condicions d'ús</strong>
                                                </strong></p>

                                            <p>
                                                <strong><strong>1.-</strong>
                                                </strong>
                                                Amb aquestes Condicions d'ús, Agrupació Tèxtil Fagepi, regula l'accés a aquesta pàgina web, i la posada a disposició de la informació que hi ha, per part dels seus clients i altres usuaris de la xarxa.</p>

                                            <p>
                                                <strong><strong>2.-</strong>
                                                </strong>
                                                Pel sol ús d'aquest lloc web, l'usuari manifesta que accepta sense reserves aquestes Condicions d'ús.</p>

                                            <p>
                                                <strong><strong>3.-</strong>
                                                </strong>
                                                L'ús del lloc web i de la informació que inclou, en principi, és de caràcter gratuït, sense perjudici que aquesta circumstància pugui variar, amb la qual cosa Agrupació Tèxtil Fagepi, es compromet a publicar les noves condicions d'ús a la web en el temps més reduït possible.</p>

                                            <p>
                                                <strong><strong>4.-</strong>
                                                </strong>
                                                L'usuari s'obliga a no utilitzar el lloc web ni la informació que s'hi ofereix per fer activitats contràries a les lleis, la moral, l'ordre públic i, en general, a fer un ús conforme a aquestes condicions d'ús. Tant l'accés com la informació són responsabilitat exclusiva de qui els fa, sense que es pugui responsabilitzar Agrupació Tèxtil Fagepi, dels danys o perjudicis que es puguin derivar d'aquest accés o ús de la informació alienes a la seva voluntat.</p>

                                            <p>
                                                <strong><strong>5.-</strong>
                                                </strong>
                                                Agrupació Tèxtil Fagepi, no és responsable dels errors en accedir al lloc web o en els seus continguts i hi posarà la diligència més gran possible per tal que aquests errors no es produeixin.<br>
                                                Agrupació Tèxtil Fagepi es reserva el dret de suspendre temporalment, i sense necessitat d'avís previ, l'accessibilitat a les seves pàgines web, per l'eventual necessitat de fer operacions de manteniment, reparació, actualització o millora.</p>

                                            <p>
                                                <strong><strong>6.-</strong>
                                                </strong>
                                                Tots els continguts del lloc web (incloent-hi, sense caràcter limitatiu, bases de dades, imatges, dibuixos, gràfics, arxius de text, àudio, vídeo i programari) són propietat d'Agrupació Tèxtil Fagepi, i estan protegits per les normes nacionals o internacionals de propietat intel·lectual i industrial.<br>
                                                Les marques, rètols, signes distintius o logotips d'Agrupació Tèxtil Fagepi, que apareixen a la pàgina web són titularitat d'Agrupació Tèxtil Fagepi. Aquests criteris també fan referència al nom de domini www.fagepi.net<br>
                                                Tots els textos, dibuixos gràfics, vídeos o suports d'àudio, propietat d'Agrupació Tèxtil Fagepi, no podran ser objecte d'ulterior modificació, còpia, alteració, reproducció, adaptació o traducció per part de l'usuari o de tercers sense l'autorització expressa d'Agrupació Tèxtil Fagepi, tret que s'indiqui el contrari.<br>
                                                L'ús no autoritzat de la informació inclosa en aquest lloc web i la lesió dels drets de propietat intel·lectual o industrial donaran lloc a les responsabilitats legalment establertes.</p>

                                            <p>
                                                <strong><strong>7.-</strong>
                                                </strong>
                                                L'establiment de qualsevol hiperenllaç des d'una pagina web aliena a qualsevol de les pàgines del lloc web d'Agrupació Tèxtil Fagepi, estarà sotmès a les condicions següents:</p>



                                                    • No es permet la reproducció total ni parcial de cap dels serveis inclosos al lloc web d'Agrupació Tèxtil Fagepi.<br>

                                               • No s'establiran deep-links amb el lloc web d'Agrupació Tèxtil Fagepi, ni dels seus serveis, ni es crearà cap explorador web o border environment.<br>

                                                • No s'inclourà cap manifestació falsa, inexacta o incorrecta sobre les pàgines del lloc web d'Agrupació Tèxtil Fagepi, i els seus serveis en particular, excepte aquells signes que formin part del mateix hiperenllaç a la pàgina web d'Agrupació Tèxtil Fagepi, no contindrà cap marca, nom comercial, rètol d'establiment, denominació, logotip, eslògan o altres signes distintius que pertanyin a Agrupació Tèxtil Fagepi.<br>

                                                • Sota cap circumstància, Agrupació Tèxtil Fagepi, serà responsable dels continguts o serveis posats a disposició del públic a la pàgina web des de la qual es faci l'hiperenllaç ni de les informacions o manifestacions que s'hi incloguin.<br>

                                                • Qualsevol hiperenllaç a la web de Agrupació Tèxtil Fagepi, s'efectuarà a la pàgina principal.



                                            <p>
                                                <strong class="resaltat_nivell_1">8.-</strong>
                                                Política de privadesa o protecció de dades</p>



                                                    <strong><strong>A. Dret d'informació</strong>
                                                    </strong>
                                                    <br>
                                                    Aquesta política de protecció de dades regula l'accés i l'ús del servei del lloc web www.fagepi.net i la resta de llocs web que Agrupació Tèxtil Fagepi, posa a disposició dels usuaris d'internet interessats en els seus serveis i continguts.<br>
                                                    De conformitat amb el que estableix la Llei orgànica 15/1999, de 13 de desembre, de protecció de dades de caràcter personal (LOPD), Agrupació Tèxtil Fagepi, titular del lloc web, informa l'usuari d'aquest lloc de l'existència d'un fitxer automatitzat de dades de caràcter personal creat per Agrupació Tèxtil Fagepi i sota la seva responsabilitat.



                                            <p>
                                            </p>


                                                    <strong><strong>B. Finalitat</strong>
                                                    </strong>
                                                    <br>
                                                    Les dades dels usuaris registrats a través dels formularis habilitats a aquest efecte en el lloc web són recaptades per Agrupació Tèxtil Fagepi, amb la finalitat de facilitar la prestació dels serveis que Agrupació Tèxtil Fagepi, proporciona a través d'aquest lloc web.

                                            <p>
                                            </p>
                                                    <strong><strong>C. Caràcter obligatori o facultatiu de la informació facilitada per l'usuari i veracitat de les dades</strong>
                                                    </strong>
                                                    <br>
                                                    Els camps obligatoris, normalment marcats amb un asterisc (*) en els formularis que ha d'emplenar l'usuari són estrictament necessaris per atendre la vostra petició; la inclusió de dades en els camps restants és voluntària.<br>
                                                    L'usuari garanteix que les dades personals facilitades a Agrupació Tèxtil Fagepi, són veraces i es fa responsable de comunicar-li qualsevol modificació.



                                            <p>
                                            </p>


                                                    <strong><strong>D. Consentiment de l'usuari</strong>
                                                    </strong>
                                                    <br>
                                                    L'enviament de dades personals amb l'ús dels formularis electrònics d'Agrupació Tèxtil Fagepi, o, si escau, missatges de correu electrònic, suposa el consentiment exprés del remitent al tractament automatitzat de les dades incloses en els serveis i productes relacionats amb les finalitats dels llocs web d'Agrupació Tèxtil Fagepi, i l'enviament de comunicacions via electrònica amb informació relacionada amb Agrupació Tèxtil Fagepi, i les seves iniciatives.</li>



                                            <p>
                                            </p>

                                                    <strong><strong>E. Butlletins i comunicacions electròniques</strong>
                                                    </strong>
                                                    <br>
                                                    Agrupació Tèxtil Fagepi, podrà posar a la disposició dels usuaris que prèviament ho hagin autoritzat a través del formulari de subscripció habilitat a aquest efecte el servei d'enviament d'un butlletí, en el qual s'inclouen les notícies, novetats i informació més rellevants del lloc web. A més, l'usuari podrà donar-se de baixa o modificar les dades de subscripció a l'esmentat butlletí a través del formulari interactiu inclòs en el lloc web.<br>
                                                    D'altra banda, Agrupació Tèxtil Fagepi, informa que remetrà comunicacions comercials per mitjans electrònics amb informació sobre altres productes, serveis i esdeveniments que puguin ser d'interès per als usuaris del lloc web quan així ho sol·licitin expressament. L'usuari podrà revocar aquest consentiment en qualsevol moment.</li>


                                            <p>
                                            </p>


                                                    <strong><strong>F. Seguretat</strong>
                                                    </strong>
                                                    <br>
                                                    Agrupació Tèxtil Fagepi, manté els nivells de seguretat de protecció de dades personals conforme al Reial decret 994/1999, d'11 de juny, relatiu a les mesures de seguretat dels fitxers automatitzats que incloguin dades de caràcter personal, i ha establert tots els mitjans tècnics al seu abast per evitar la pèrdua, mal ús, alteració, accés no autoritzat i robatori de les dades que l'usuari faciliti a través del lloc web, sense perjudici d'informar-lo que les mesures de seguretat a internet no són inexpugnables.<br>
                                                    Agrupació Tèxtil Fagepi, es compromet a complir amb el deure secret i la confidencialitat respecte de les dades personals incloses en el fitxer automatitzat d'acord amb la legislació aplicable i a conferir-los un tractament segur en les cessions i transferències internacionals de dades que, si escau, es puguin produir.</li>


                                            <p>
                                            </p>

                                                    <strong><strong>G. Cookies i IPs</strong>
                                                    </strong>
                                                    <br>
                                                    Cookies L'usuari accepta l'ús de cookies i seguiments d'IP l'ús dels quals permet a Agrupació Tèxtil Fagepi a recollir dades a efectes estadístics com: data de la primera visita, nombre de vegades que s'ha visitat, data de l'última visita, URL i domini de la qual prové, explorador utilitzat i resolució de la pantalla. No obstant això, l'usuari si ho desitja pot desactivar i / o eliminar aquestes cookies seguint les instruccions del seu navegador d'Internet.<strong> </strong>


                                            <br>
                                                            <strong>Cookies de personalització</strong>
                                                            <br>
                                                            Les cookies de personalització emmagatzemen les preferències de l'usuari perquè el comportament de la pàgina web es mantingui entre una visita i una altra. A la nostra web les utilitzem per: Mantenir l'idioma en què es visualitza la pàgina web entre les diferents visites del mateix usuari. Recordar si un usuari ha acceptat la política de cookies per mostrar o no l'avís corresponent. Altres variables de sessió per facilitar la navegació pel lloc web.

                                            <br>
                                                            <strong>Cookies d'anàlisi </strong>
                                                            <br>
                                                            Recopila informació totalment anònima sobre la navegació dels usuaris pel lloc web amb la finalitat de conèixer l'origen de les visites i altres dades estadístiques que ens permetin millorar la navegació, continguts i experiència dels usuaris al nostre web. Aquesta informació és recollida pel servei de Google Analytics per generar les estadístiques necessàries. Són les següents:<br>
                                                            <strong>__utma: </strong>
                                                            Aquesta galeta se sol instal · lar al navegador en la seva primera visita al lloc web.<br>
                                                            <strong>__utmz:</strong>
                                                            Emmagatzema el tipus de referència utilitzat per arribar a aquest lloc web<br>
                                                            <strong>__utmb:</strong>
                                                            Utilitzada per establir i continuar una sessió d'usuari amb aquest lloc web<br>
                                                            <strong>__utmc:</strong>
                                                            Opera juntament amb la galeta __ UTMB per determinar si procedeix establir una nova sessió<br>
                                                            Més informació al <a href="http://www.google.es/intl/es_ALL/analytics/learn/privacy.html" target="_blank">Centre de privadesa de Google</a>


                                            <br>
                                                            En altres llocs de xarxes socials on tenim presència també s'instal·len cookies de tercers a tots els seus visitants. Poden ser els següents:<br>
                                                            <strong>LinkedIn. </strong>
                                                            Cookies: jsessionid, visit, bcookie, S-LI-IDC, L1e, X-LI-IDC, _qca, RT, __ UTMA, __ UTMB, __ utmc, __ utmz, __ utmv, leo_auth_token, lang. <a href="http://www.linkedin.com/legal/cookie-policy?trk=hb_ft_cookie" target="_blank">Política de cookies de LinkedIn.</a>
                                                            <br>
                                                            <strong>Google+, maps i local. </strong>
                                                            Cookies: test_cookie, NID, OTZ, PREF.<a href="https://www.google.es/intl/es/policies/technologies/types/" target="_blank"> Política de Cookies de Google.</a>
                                                            <br>
                                                            <strong>Facebook. </strong>
                                                            Cookies: datr, reg_fb_gate i reg_fb_ref. <a href="https://www.facebook.com/help/cookies" target="_blank">Política de cookies de Facebook.</a>



                                                            <p>
                                                                <strong>Com desinstal lar les cookies </strong>
                                                                <br>
                                                                La informació que ens proporcionen les cookies és valuosa perquè ens ajuda a millorar el servei que oferim als nostres usuaris. Agraïm l'acceptació de les cookies del nostre lloc web però, en qualsevol cas, és possible configurar el navegador per acceptar, o no, les cookies que enviem o perquè el navegador avisi quan un servidor vulgui guardar una cookie. Aquí algunes instruccions per a diferents navegadors:</p>

                                                            <ul>

                                                                    <strong>Google Chrome:</strong>
                                                                    https://support.google.com/chrome/answer/95647<br>
                                                                    Firefox: <a href="http://support.mozilla.org/kb/delete-cookies-remove-info-websites-stored" target="_blank">http://support.mozilla.org/kb/delete-cookies-remove-info-websites-stored</a>



                                                                    <strong>Internet Explorer:</strong>
                                                                    <a href="http://windows.microsoft.com/es-es/internet-explorer/delete-manage-cookies" target="_blank">http://windows.microsoft.com/es-es/internet- explorer/delete-manage-cookies</a>



                                                                    <strong>Safari:</strong>
                                                                    <a href="http://www.csa.cat/altres/support.apple.com/kb/ph11920" target="_blank">http://support.apple.com/kb/ph11920</a>



                                                                    <strong>Altres:</strong>
                                                                    En general, per eliminar les cookies del navegador es pot realitzar des del menú Arranjament o Configuració del navegador i buscar la secció Privacitat. Allà es troben les opcions per gestionar les cookies i altres dades emmagatzemades pels llocs web visitats, així com configurar el comportament del navegador en tractar les cookies.</li>

                                                            </ul>



                                                    </ul>

                                                </li>

                                            </ul>

                                            <p>
                                            </p>

                                            <ul>

                                                    <strong><strong>H. Spamming</strong>
                                                    </strong>
                                                    <br>
                                                    Agrupació Tèxtil Fagepi, no utilitza tècniques de "spamming" i únicament tractarà les dades que l'usuari transmeti a través dels formularis electrònics habilitats en aquest lloc web o missatges de correu electrònic.<br>
                                                    El tractament de les dades de caràcter personal i l'enviament de comunicacions comercials fetes per mitjans electrònics són conformes a la Llei orgànica 15/1999, de 13 de desembre, de protecció de dades de caràcter personal (BOE de 14 de desembre de 1999) i a la Llei 34/2002, d'11 de juliol, de serveis de la societat d'informació i de comerç electrònic (BOE de 12 de juliol de 2002).

                                            </ul>

                                            <p>
                                            </p>



                                                    <strong><strong>I. Dret d'oposició, correcció i actualització de dades</strong>
                                                    </strong>
                                                    <br>
                                                    L'usuari té dret a accedir a aquesta informació, rectificar-la, si les dades són errònies, i donar-se de baixa dels serveis d'Agrupació Tèxtil Fagepi.<br>
                                                    Aquests drets es poden fer efectius amb la mateixa configuració de la pàgina web. En cas de problemes per a l'execució efectiva en línia i per a qualsevol tipus de dubte o controvèrsia respecte a la nostra política de privadesa de dades us podreu adreçar directament a: <a href="mailto:central@fagepi.net" target="_blank">central@fagepi.net</a> o be a l'adreça postal:Avinguda Mestre Muntaner, 86, 08700-Igualada (Barcelona).



                                            <p>
                                                <strong><strong>9.-</strong>
                                                </strong>
                                                Les condicions d'ús d'aquest lloc web tenen caràcter indefinit. Agrupació Tèxtil Fagepi, en tot cas, es reserva el dret unilateral de modificar les condicions d'accés, i els continguts que s'hi inclouen, o les condicions d'ús del seu servei d'allotjament gratuït d'articles, incloses en aquest document.</p>

                                            <p>
                                                <strong><strong>10.-</strong>
                                                </strong>
                                                La prestació del servei d'aquest lloc web i aquestes condicions d'ús es regeixen per la llei espanyola. Les parts, amb renúncia expressa a qualsevol altre fur que els pogués correspondre, acorden sotmetre's a la jurisdicció dels Jutjats i Tribunals de Barcelona.</p>

                                        </td></tr></tbody></table></div>
                            
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-4 -->
            <div class="col-md-8">
                <div class="widget-contactform">

                </div><!-- /.col-md-8 -->
            </div><!-- /.widget-contactform -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /flat-row portfolio-row-page -->


<?php $_SESSION['msj'] = null ?>
