<?php $n = 0; ?>
<?php foreach($list as $ns=>$p): ?>
<?php if($n==0): ?>
<div class="row">
<?php endif ?>
<div class="col-md-4 col-sm-12">
        <div class="flat-event">
            <div class="event-img" style="background:url(<?= $p->foto ?>); background-size:cover;">
                <img src="<?= $p->foto ?>" alt="images" style="visibility: hidden; height:266px;">
            </div>
            <?php if(!empty($p->presentacion)): ?>
                <?php if(get_instance()->router->fetch_method()=='presentacions'): ?>
                    <?php 
                        $presentacion_link = !empty($_SESSION['user'])?$p->presentacion:base_url('registro/index/add').'?redirect=seminaris';
                        $presentacion_link2 = !empty($_SESSION['user'])?$p->presentacion2:base_url('registro/index/add').'?redirect=seminaris';
                    ?>
                    <button type="button" id="buttonid<?= $p->id ?>" onclick="document.location.href='<?= $presentacion_link ?>'" class="event-presentacio-btn flat-button button-style" target="_new" style="padding:11px 20px; text-align:center;"><i class="fa fa-file-pdf-o fa-2x"></i></button>
                    <?php if(!empty($p->presentacion2)): ?>
                        <button type="button" id="buttonid<?= $p->id ?>" onclick="document.location.href='<?= $presentacion_link2 ?>'" class="event-presentacio-btn flat-button button-style" target="_new" style="padding:11px 20px; text-align:center; left:94px"><i class="fa fa-file-pdf-o fa-2x"></i></button>
                    <?php endif ?>
                    <a style="display:none" href="<?= $presentacion_link ?>" target="_new">
                        veure presentacio
                    </a>
                    <a href="javascript:savePresentacion(<?= $p->seminarios_id ?>)" title="eliminar presentacio" style="color:white; position: absolute; right: 30px; top: 40px;"><i class="fa fa-remove"></i></a>
                <?php else: ?>
                    <?php 
                        $presentacion_link = !empty($_SESSION['user'])?'javascript:savePresentacion('.$p->id.')':base_url('registro/index/add').'?redirect=seminaris';
                        $esta = get_instance()->db->get_where('presentaciones',array('seminarios_id'=>$p->id,'user_id'=>get_instance()->user->id));                        
                        $heart = $esta->num_rows()==0?'':'<i class="fa fa-heart-o"></i>';
                    ?>
                    <button type="button" id="buttonid<?= $p->id ?>" onclick="document.location.href='<?= $presentacion_link ?>'" class="event-presentacio-btn flat-button button-style"><?= $heart ?> Guardar presentació</button>
                    <a style="display:none" href="<?= $presentacion_link ?>" target="_new">
                        guardar presentacio
                    </a>
                <?php endif ?>
            <?php endif ?>
            <div class="event-content">
                <?php /*$style = strtotime(str_replace('/','-',$p->fecha))<strtotime(date("Y-m-d"))?'style="text-decoration: line-through #ff0000;"':'';*/ ?>
                <?php $style = strtotime(str_replace('/','-',$p->fecha))<strtotime(date("Y-m-d"))?'subrayado':''; ?>
                <div class="flat-time <?= $style ?>">                    
                    <span class="day"><?= date("d",strtotime(str_replace('/','-',$p->fecha))) ?></span>
                    <span class="month"><?= strftime("%b",strtotime(str_replace('/','-',$p->fecha))) ?></span>
                    <span class="hours"><?= $p->hora ?></span>
                </div>
                <div class="event-title">
                    <h4 class="title"><a href="<?= site_url('seminaris/'.toURL($p->id.'-'.$p->titulo)) ?>"><?= $p->titulo ?></a></h4>
                    <p class="add"><i class="fa fa-map-marker"></i><?= $p->ubicacion ?></p>
                </div>
            </div>
        </div>
    </div><!-- /col-md-4 -->
<?php $n++; ?>
<?php if($n==3 || $ns==count($list)-1): $n = 0; ?>
</div><!-- /row -->
<?php endif ?>
<?php endforeach ?>
<?php if(count($list)==0): ?>
    <!--No se encuentran seminarios pendientes por ahora.-->
<?php endif ?>
