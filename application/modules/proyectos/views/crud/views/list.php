<?php if(isset($list)): ?>
    <?php $x = 0; ?>
    <?php foreach($list as $num_row => $p): ?>
        <?php if($x==0): ?>
        <div class="row" style="margin-left:0px; margin-right:0px;">
        <?php endif ?>            
            <div class="post">
                <div class="wrap-post">
                    <div class="img-recent-causes" style="background:url('<?= $p->foto ?>') no-repeat; background-position:50% 0; background-size:cover;">
                        <a href="<?= $p->link ?>" style=""><img src='<?= $p->foto ?>' style="visibility:hidden;"></a>
                    </div>
                    <div class="post-recent-causes">
                        <div class="title-later-new">
                            <p> <?= $p->s379c07ff ?></p>
                        </div> 
                        <h6 class="title-post">
                            <?php $ponts = strlen($p->titulo)>46?'...':''; ?>
                            <a href="<?= $p->link ?>"><?= substr($p->titulo,0,46).$ponts ?></a>
                        </h6>
                    </div>
                   <div class="entry-content">
                        <div class="donation-v1">
                            <p><?= cortar_palabras(strip_tags($p->descripcion_corta),45) ?></p><span></span>
                        </div>

                    </div>
                    <div class="flat-progress">
                        <div class="perc"><?= mb_strtolower($p->empresa,mb_detect_encoding($p->empresa)) ?> </div>
                        <div class="progress-bar" data-percent="100" data-waypoint-active="yes">
                            <div class="progress-animate"></div>
                        </div>
                    </div><!-- /flat-progress -->
                    <div class="more">
                        <a class="visitas" title="Visitas"><i class="icon-eye"></i>  <?= empty($p->visitas)?0:$p->visitas ?><span class="hidden">vistas</span></a>
                        <a class="likes" title="Likes" id='likes<?= $p->id ?>' href="javascript:sumarCorazon('<?= $p->id ?>')"><i class="icon-heart"></i>  <?= $p->likes ?></a>
                        <button onclick="document.location.href='<?= $p->link ?>'" class="flat-button button-style">Veure Projecte</button>
                    </div>
                </div>                        
            </div>
            <?php $x++; ?>
        <?php if($x==3): ?>
        <?php $x = 0; ?>
        </div>        
        <?php endif ?>
    <?php endforeach ?> 
<?php endif; ?>
