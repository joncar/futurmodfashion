<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function formularios(){
            $crud = $this->crud_function('','');                  
            $crud->add_action('Reservas','',base_url('reservas/admin/reservas/').'/');
            $crud->columns('titulo','fecha_cierre','plazas','plazas_disponibles','enlace');
            $crud->callback_column('plazas_disponibles',function($val,$row){
                return (string)($row->plazas-$this->db->get_where('reservas',array('formularios_id'=>$row->id,'status'=>3))->num_rows());
            });
            $crud->callback_column('enlace',function($val,$row){
                return base_url('reservas/'.toUrl($row->id.'-'.$row->titulo));
            });
            $this->loadView($crud->render());
        }
        
        function reservas($x){
            if(is_numeric($x)){
                $crud = $this->crud_function('',''); 
                $crud->where('formularios_id',$x)
                     ->field_type('formularios_id','hidden',$x);     
                $crud->field_type('status','dropdown',array('1'=>'Solicitada','2'=>'Rechazado','3'=>'Aprobada'))
                ->columns('nombre','empresa','entidad','email','status');
                $crud->callback_after_update(function($post){
                    if($post['status']==3 && $post['enviar_correo']==1){
                        $formulario = get_instance()->db->get_where('formularios',array('id'=>$post['formularios_id']));
                        if($formulario->num_rows()>0){
                            $post['fecha'] = strftime('%d %B %Y',strtotime($formulario->row()->fecha_cierre));
                        }
                        get_instance()->enviarcorreo($post,8,$post['email']);
                    }
                });
                $this->loadView($crud->render());
            }
        }
    }
?>
