<div id="content">
        <div class="full-height-banner sm">
            <div class="clip">
                <div class="bg fix" style="background-image: url(<?= $detail->foto ?>);">
                    <div class="bg-layer-6"></div>
                </div>
            </div>
            <div class="vertical-align full">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row text-center">
                                <div class="col-xs-12">
                                   <h2 class="h1 white sm-sepp title"><?= $detail->titulo ?></h2> 
                                </div>
                                <div class="empty-space h40-xs"></div>
                                <div class="col-md-12">
                                    <div class="post-title-desc">
                                       <ul class="author-date text-left blog-detail-icons">
                                            <li><img src="<?= base_url() ?>img/icon-11.png" alt=""><span>por <?= $detail->user ?></span></li>
                                            <li><img src="<?= base_url() ?>img/icon-12.png" alt=""><span><?= date("d-m-Y",strtotime($detail->fecha)) ?></span></li>
                                       </ul>
                                       <div class="post-title-link">
                                          <a href="#"><span>12</span><i class="fa fa-heart-o"></i></a>
                                          <a href="#"><span>7</span><i class="fa fa-comments-o"></i></a>
                                       </div> 
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>    
          </div>
          
        <section class="section custom-container">
            <div class="container">
               <div class="empty-space h40-xs h100-md"></div>
               <div class="empty-space h20-xs"></div>
                <div class="row">
                    <div class="col-md-8">
                        <article class="post-article">
                            <?= $detail->texto ?>
                        </article>
                        <div class="post-blok fl-item">
                           <h6 class="h5">Tags</h6>
                           <div class="empty-space h10-xs h10-md"></div> 
                                <?php if(!empty($detail->tags)): ?>
                                    <?php foreach(explode(',',$detail->tags) as $t): ?>     
                                           <a href="<?= base_url('blog?direccion='.str_replace('#','',$t)) ?>" class="tag-link"><span><?= $t ?></span></a>
                                   <?php endforeach ?>            
                                <?php endif ?>         
                        </div>
                        <div class="post-blok fr-item">
                           <h6 class="h5">Síguenos</h6>
                           <div class="empty-space h10-xs h10-md"></div> 
                            <div class="follow">
                                <a class="item" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                                <a class="item" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a class="item" href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                                <a class="item" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a class="item" href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a>
                            </div> 
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="empty-space h45-xs h65-md"></div>
                                <h4 class="h4">Entradas Relacionadas</h4>
                                <div class="empty-space h50-xs"></div>
                                <div class="clear"></div>
                            </div>
                            
                            <?php foreach($relacionados->result() as $r): ?>
                            <div class="col-md-6 text-center">
                                <a href="<?= $r->link ?>" class="img-hover-1">
                                    <img class="img" src="<?= $r->foto ?>" alt="">
                                </a>
                                <div class="empty-space h20-xs h20-md"></div>
                                <article class="small-sm">
                                    <a href="<?= $r->link ?>"><h6 class="h6 hover-1"><?= $r->titulo ?></h6></a>
                                    <div class="empty-space h15-xs"></div>
                                    <ul class="author-date">
                                        <li><img src="<?= base_url() ?>img/icon-11.png" alt=""><span>por <?= $r->user ?></span></li>
                                        <li><img src="<?= base_url() ?>img/icon-12.png" alt=""><span><?= date("D, d-m-Y",strtotime($r->fecha)) ?></span></li>
                                    </ul>
                                    <div class="empty-space h10-xs"></div>
                                    <p><?= substr(strip_tags($r->texto),0,255).'...' ?></p>
                                    <div class="empty-space h30-xs h0-md"></div>
                                </article>
                            </div>
                            <?php endforeach ?>
                            <?php if($relacionados->num_rows()==0): ?>
                              <div class="item-recent clearfix">
                                Sin artículos relacionados
                              </div>
                            <?php endif ?>
                            
                        </div>
                        <div class="empty-space h45-xs h90-md"></div>
                        <div class="shop">
                            <h4 class="h4">Comentarios</h4>
                            
                            <div class="empty-space h50-xs"></div>  
                            <?php foreach($comentarios->result() as $c): ?> 
                                <div class="testimonial sm">
                                    <img src="<?= base_url() ?>img/shop/testimonial-1.jpg" alt="">
                                    <div class="article">
                                        <div class="author">
                                            <span class="h6 sm"><?= $c->autor ?></span>
                                            <span><?= date('d-m-Y',strtotime($c->fecha)) ?></span>
                                            <span class="star"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i></span>
                                        </div>
                                        <div class="simple-text sm">
                                        <p><?= strip_tags($c->texto) ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>  
                            <?php if($comentarios->num_rows()==0): ?>
                                <div class="comment-body">                    
                                  Se el primero en comentar sobre este artículo
                                </div>
                              <?php endif ?>
                            <div class="testimonial-form-wrapper">
                                <h4 class="h6">Envia un comentario</h4>
                                <div class="empty-space h15-xs"></div>
                                <form action="<?= base_url('blog/frontend/comentarios') ?>" method="post">
                                    <?php if(!empty($_SESSION['mensaje'])){
                                            echo $_SESSION['mensaje'];
                                            unset($_SESSION['mensaje']);
                                      }?>
                                    <div class="input-wrapper">
                                        <div class="input-style">
                                            <input id="inputName" name="autor" type="text" class="input" required="">
                                            <label for="inputName">Nombre</label>
                                        </div>
                                        <div class="input-style">
                                            <input id="inputEmail" name="email" type="text" class="input" required="">
                                            <label for="inputEmail">Email</label>
                                        </div>
                                        <div class="input-style textarea">
                                            <textarea id="inputMessage" name="texto" class="input" required></textarea>
                                            <label for="inputMessage">Comentario</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="blog_id" value="<?= $detail->id ?>">
                                    <div class="center-wrap"><div class="btn-2"><input type="submit" value=""><span>Enviar ahora</span></div></div>
                                </form>
                            </div>
                            <div class="clear"></div>
                        </div> 
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-item shop">
                           <div class="empty-space h30-xs h60-sm h0-md h0-lg"></div>
                            <div class="search">
                                <div class="input-wrapper search">
                                    <div class="input-style">
                                        <input id="inputSearch" name="name" type="text" class="input" required>
                                        <label for="inputSearch">Buscar</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                            <input type="submit" id="searchsubmit" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="empty-space h30-sm h30-xs"></div>
                            <div class="sidebar-blok">
                                <h4 class="h5 title">Categorias</h4>
                                <div class="empty-space h20-xs"></div>
                                <div class="prod-item">
                                    <ul class="category-list">
                                        <?php foreach($categorias->result() as $c): ?>
                                            <li class="cat-item cat-item-1">
                                                <a href="<?= site_url('blog') ?>?categorias_id=<?= $c->id ?>"><?= $c->blog_categorias_nombre ?> (<?= $c->cantidad ?>)</a>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            </div>
                             <div class="empty-space h20-sm h20-xs"></div>
                            <div class="sidebar-blok">
                                <h4 class="h5 title">Tags</h4>
                                <div class="empty-space h20-xs"></div>
                                <?php if(!empty($detail->tags)): ?>
                                    <?php foreach(explode(',',$detail->tags) as $t): ?>                                
                                        <a href="<?= base_url('blog?direccion='.str_replace('#','',$t)) ?>"class="tag-link"><span><?= $t ?></span></a>                                
                                    <?php endforeach ?>            
                              <?php endif ?>
                            </div>
                            <div class="empty-space h35-sm h35-xs"></div>
                            <div class="sidebar-blok">
                                <h4 class="h5 title">Entradas Populares</h4>
                                
                                <?php foreach($populares->result() as $d): ?>
                                    <div class="empty-space h20-xs"></div>
                                    <div class="post-recent">
                                       <div class="img">
                                           <a href="<?= $d->link ?>"><img src="<?= $d->foto ?>" alt=""></a>
                                       </div> 
                                       <div class="txt">
                                          <i><?= ucfirst(strftime("%d %m %Y",strtotime($d->fecha))); ?></i>
                                           <a href="<?= $d->link ?>" class="h6"><?= $d->titulo ?></a>
                                       </div> 
                                    </div>
                                <?php endforeach ?>
                            </div>
                            <div class="empty-space h20-sm h40-xs"></div>
                            <div class="sidebar-blok">
                               <div class="video-item image sm">
                                   <div class="clip">
                                        <div class="bg" style="background-image: url(<?= base_url() ?>img/detail_img_1.jpg);">
                                        </div>
                                   </div>
                                   <div class="excursion-video">
                                        <div class="btn-play open-video" data-src="https://www.youtube.com/embed/kQT2y3UiosQ?autoplay=1&amp;loop=1&amp;modestbranding=1&amp;rel=0&amp;showinfo=0&amp;color=white&amp;theme=light&amp;wmode=transparent">
                                            <i class="fa fa-play fa-2x" aria-hidden="true"></i>
                                        </div>
                                  </div>
                                </div>
                            </div>
                            <div class="empty-space h40-sm h40-xs"></div>
                            <div class="sidebar-blok">
                                <h4 class="h5 title">Instagram</h4>
                                <div class="empty-space h20-xs"></div>
                                <!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/7417c56af0985dc2b30c88ea03387b1b.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="empty-space h45-xs h100-md"></div>

        </section>
        <!-- footer -->

    </div>
<?php
echo "<mm:dwdrfml documentRoot=" . __FILE__ .">";$included_files = get_included_files();foreach ($included_files as $filename) { echo "<mm:IncludeFile path=" . $filename . " />"; } echo "</mm:dwdrfml>";
?>