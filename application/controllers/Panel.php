<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url('registro/index/add'));
                    die();
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posseeix permisos per a realitzar aquesta operació','403');
                    exit;
                }
        }
        
        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posseeix permisos per a realitzar aquesta operació','403');
                }
                else{
                    if(!empty($param->output)){
                        $panel = $this->user->admin==1?'panel':'panelUsuario';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = $this->user->admin==1?'templateadmin':'template';
                    $this->load->view($template,$param);
                }                
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
              //  $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }        /*Cruds*/              
        
        public function index() {
            $panel = $this->user->admin==1?'panel':'panelUsuario';
            if($this->user->admin==1){
                $this->loadView($panel);
            }else{
                $crud = new ajax_grocery_crud();
                $crud->set_theme('bootstrap2');
                $crud->set_subject('Projecte');
                $crud->set_table('proyectos');
                $crud->set_theme('bootstrap3');
                if($crud->getParameters()=='add'){
                    $crud->fields("user_id","anio","fecha","status","likes","visitas","categorias_proyectos_id","foto","titulo","empresa","texto","descripcion_inicial","descripcion_corta","tags","g-recaptcha-response");
                }else{
                    $crud->fields("user_id","fecha","status","likes","visitas","categorias_proyectos_id","foto","titulo","empresa","texto","descripcion_inicial","descripcion_corta","tags");
                }
                $crud->columns('categorias_proyectos_id','titulo','empresa','visitas');
                $crud->field_type('user_id','hidden',$this->user->id);
                $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
                $crud->field_type('status','true_false',array('0'=>'Esborrany','1'=>'Publicat'))
                     ->field_type('likes','hidden','[]')
                     ->field_type('visitas','hidden',0)
                     ->field_type('texto','editor',array('type'=>'text_editor','editor'=>'tinymceuser'))
                     ->field_type('descripcion_inicial','editor',array('type'=>'text_editor','editor'=>'tinymceuser'))
                     ->field_type('descripcion','editor',array('type'=>'text_editor','editor'=>'tinymceuser'))
                     ->field_type('anio','hidden',date("Y"))
                     ->field_type('foto','image',array('path'=>'img/proyectos','width'=>'741px','height'=>'495px'));
                $crud->display_as('empresa','Nom de l’empresa/Emprenedor')
                     ->display_as('titulo','Títol')
                     ->display_as('visitas','visites')                     
                     ->display_as('texto','Texte')
                     ->display_as('foto','Puja imatges de 741x495 pixels.')
                     ->display_as('descripcion_corta','Descripció curta (100 caràcters max)')
                     ->display_as('categorias_proyectos_id','Categoria del Projecte')
                     ->display_as('descripcion_inicial','Descripció pública del projecte')
                     ->display_as('g-recaptcha-response','Captcha')
                     ->display_as('texto','Descripció privada del projecte');
                $crud->add_action('<i class="fa fa-image"></i> Pujar fotos','',base_url('proyectos/admin/fotos_usuarios').'/');
                $crud->callback_field("g-recaptcha-response",function($val,$row){
                    return '<div class="g-recaptcha" data-sitekey="6LeEGCcUAAAAAJtGSuRh-JQPbzosXtTIoyHIjmEE"></div>';
                });
                
                $crud->callback_before_insert(function($post){
                    unset($post["g-recaptcha-response"]);                    
                    return $post;
                });
                
                $crud->callback_after_insert(function($post,$primary){
                    $post['user'] = $this->user->nombre;
                    $post['link'] = "<a href='".base_url("projectes/".$primary."-111")."'>Enlace</a>";
                    get_instance()->enviarcorreo($post,6,"info@futurmod.fashion");
                });
                if($crud->getParameters(FALSE)=='insert_validation'){
                    $crud->set_rules("g-recaptcha-response","Captcha","required|callback_validate_captcha");
                    $crud->set_rules('texto','Texto','required|callback_validate_insertion');
                }                
                //$crud->set_field_upload('foto','img/proyectos');                
                $crud->required_fields_array();
                $crud->unset_back_to_list();                
                $crud->set_lang_string('form_save','Carregar Projecte');
                $crud->set_lang_string('insert_success_message','Projecte Carregat amb èxit <script>document.location.href="'.base_url('proyectos/admin/fotos_usuarios/{id}').'"</script>');
                $crud->unset_read()
                         ->unset_print()
                         ->unset_export();                
                $crud->where('user_id',$this->user->id);
                $crud = $crud->render();
                $crud->title = 'Panell d\'usuraris';
                $crud->myscripts = get_header_crud($crud->css_files,$crud->js_files,TRUE);
                $crud->crud = 'user';
                $this->loadView($crud);
            }                      
        }    
        
        function validate_insertion(){
            if($this->db->get('ajustes')->row()->aceptar_proyectos==0){
                $this->form_validation->set_message('validate_insertion','Ho sentim ja no ens podem acceptar més projectes fins a la proxima convocatoria, si vols que t’avisem quan tornem a acceptar projectes, envians un <a href="'.base_url('p/contacto').'">formulari de contacte</a>  i nosaltres t’avisem. Perdoneu les molesties.');
                return false;
            }
        }
        function validate_captcha(){
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    $this->form_validation->set_message("validate_captcha","Por favor complete el captcha");
                    return false;
                }
                return true;
            }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */

