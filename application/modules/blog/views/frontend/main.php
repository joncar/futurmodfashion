<div class="page-title parallax parallax4" style=" background-repeat: no-repeat; background-position: 50% 0px; background-image: url(../../img/parallax/bg-parallax4_6.jpg);  background-size: inherit;" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Blog</h2>
                </div><!-- /.page-title-heading -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><a href="<?= site_url() ?>">Home</a></li>
                        <li>Blog</li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title parallax -->

<section class="main-content blog-post v1">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="post-wrap">

                    <?php foreach ($detail->result() as $d): ?>
                        <article class="post">
                            <div class="entry-image">
                                <?php if (!empty($d->foto)): ?>
                                    <img src="<?= $d->foto ?>" alt="image">
                                <?php endif ?>
                            </div>
                            <div class="content-post">
                                <div class="title-education">
                                    <p><?= $d->categorias->row(0)->blog_categorias_nombre ?></p>
                                </div>
                                <h4 class="title-post">
                                    <a href="<?= $d->link ?>"><?= $d->titulo ?></a>
                                </h4>
                                <div class="entry-meta">                              
                                    <span class="author"><a href="#"><?= $d->user ?></a></span>
                                    <span class="date"><a href="#"><?= ucfirst(strftime("%d-%m-%Y", strtotime($d->fecha))); ?></a></span>
                                    <span class="comment"><a href="#">0 Comments</a></span>
                                </div><!-- /.entry-meta -->

                                <div class="entry-content">
                                    <p><?= substr(strip_tags($d->texto), 0, 255) . '...' ?></p>
                                </div><!-- /entry-post -->
                                <div class="more">
                                    <a href="<?= $d->link ?>">Llegir Més</a>
                                </div>
                                <ul class="flat-socials">
                                    <li class="facebook">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $d->link ?>">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="twitter">
                                        <a href="http://www.twitter.com/share?url=<?= $d->link ?>">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>

                                </ul>
                            </div><!-- /content-post -->
                        </article><!-- /post -->
                    <?php endforeach ?>


                    <div class="blog-pagination">
                        <ul class="flat-pagination clearfix">
                            <li class="prev">
                                <a href='javascript:changePage("<?= empty($_GET['page']) ? 1 : ($_GET['page'] - 1) ?>")'>
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>
                            <?php for ($i = 1; $i <= $total_pages; $i++): ?>
                                <li><a href='<?= base_url().'blog?page='.$i ?>' class='<?= $i == $current_page ? 'active' : '' ?>'><?= $i ?></a></li>
                            <?php endfor ?>                                
                            <li class="next">
                                <a href='javascript:changePage("<?= empty($_GET['page']) ? 1 : ($_GET['page'] + 1) ?>")'>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.blog-pagination -->
                </div><!-- /post-wrap -->
            </div><!-- /col-md-8 -->

            <div class="col-md-4">
                <div class="sidebar">
                    <div class="widget widget-search">
                        <form action="<?= base_url('blog') ?>" id="searchform" method="get">
                            <div>
                                <input type="text" id="s" name="direccion" class="sss" placeholder="Buscar">
                                <input type="hidden" id="page" name="page" value="<?= !empty($_GET['page']) ? $_GET['page'] : '1' ?>">
                                <input type="hidden" id="categorias_proyectos_id" name="categorias_proyectos_id" value="<?= !empty($_GET['categorias_proyectos_id']) ? $_GET['categorias_proyectos_id'] : '' ?>">
                                <input type="submit" value="" id="searchsubmit">
                            </div>
                        </form>
                    </div><!-- /widget-search -->

                    <div class="widget widget-recent-posts">
                        <h5 class="widget-title">Noticies Populars</h5>
                        <ul class="recent-posts clearfix">

                            <?php foreach ($populares->result() as $d): ?>
                                <li>
                                    <div class="thumb">
                                        <img src="<?= $d->foto ?>" alt="image" style="width:84px">
                                    </div>
                                    <div class="text">
                                        <a href="<?= $d->link ?>"><?= $d->titulo ?></a>
                                        <p><?= ucfirst(strftime("%d/%m/%Y", strtotime($d->fecha))); ?></p>
                                    </div>
                                </li>
                            <?php endforeach ?> 
                        </ul><!-- /popular-news clearfix -->
                    </div><!-- /widget widget-recent-posts -->

                    <div class="widget widget-categories">
                        <h5 class="widget-title">Categories</h5>
                        <ul class="categories">

                            <?php if ($categorias->num_rows() == 0): ?>
                                <li>
                                    <a href="#">Sense categories</a>
                                </li>
                            <?php endif ?>
                            <?php foreach ($categorias->result() as $c): ?>
                                <li><a href="<?= site_url('blog?blog_categorias_id=' . $c->id) ?>"><?= $c->blog_categorias_nombre ?> (<?= $c->cantidad ?>)</a></li>
                            <?php endforeach ?>

                        </ul>
                    </div><!-- /widget-categories -->

                    <div class="widget widget-tags">
                        <h5 class="widget-title">Tags</h5>
                        <ul class="tag-list">
                            <?php if (!empty($detail->tags)): ?>
                                <?php foreach (explode(',', $detail->tags) as $t): ?>
                                    <li><a href="<?= base_url('blog?direccion=' . str_replace('#', '', $t)) ?>" class="tag-link"><span><?= $t ?></span></a></li>
                                            <?php endforeach ?>            
                                        <?php endif ?>
                        </ul>
                    </div><!-- /widget-tags -->


                </div><!-- /sidebar -->
            </div><!-- /col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->   
</section><!-- /main-content blog-post -->
<script>
    function changePage(id) {
        $("#page").val(id);
        $("#searchForm").submit();
    }

    function changeCategoria(id) {
        $("#blog_categorias_id").val(id);
        $("#searchForm").submit();
    }
</script>
