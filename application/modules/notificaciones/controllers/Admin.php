<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function notificaciones(){
            $crud = $this->crud_function('','');    
            $crud->add_action('Enviar boletin','',base_url('notificaciones/admin/sendBoletin').'/');

            $this->loadView($crud->render());
        }
        
        function subscritos(){
            $crud = $this->crud_function('','');            
            $this->loadView($crud->render());
        }

        function procesarFichero($id){
            $fichero = $this->db->get_where('importar_emails',array('id'=>$id));
            if($fichero->num_rows()>0){
                $fichero = $fichero->row();
                $file = 'boletins/files/'.$fichero->fichero;
                require_once APPPATH.'libraries/Excel/SpreadsheetReader.php';
                $reader = new SpreadsheetReader($file);
                //Se dbe limpiar los datos?
                if($fichero->limpiar==1){
                    $this->db->query('truncate emails');
                }
                foreach($reader as $data){
                    if($this->db->get_where('emails',array('email'=>$data[0]))->num_rows()==0){
                        $this->db->insert('emails',array(
                            'email'=>$data[0],
                            'nombre'=>$data[1]                        
                        ));
                    }
                }
                redirect('notificaciones/admin/emails/success');
            }
        }

        function importar_emails($x = '',$y = ""){            
            if($x=='procesar' && is_numeric($y)){
                $this->procesarFichero($y);
                die();
            }
            $crud = $this->crud_function('','');  
            $crud->set_field_upload('fichero','boletins/files');  
            $crud->display_as('limpiar','Limpiar y  añadir');
            $crud->add_action('Importar','',base_url('notificaciones/admin/importar_emails/procesar').'/');        
            $this->loadView($crud->render());
        }

        function emails($x = ''){            
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $crud->output = '<a href="'.base_url('notificaciones/admin/importar_emails').'" class="btn btn-info">Importar</a>'.$crud->output;
            $this->loadView($crud);
        }

        function sendBoletin($id = ''){
            if(is_numeric($id)){
                $boletin = $this->db->get_where('notificaciones',array('id'=>$id));
                if($boletin->num_rows()>0){
                    $boletin = $boletin->row();
                    $subscritos = $this->db->get_where('emails');
                    $email = file_get_contents('boletins/'.$boletin->carpeta.'/'.$boletin->fichero_html);
                    $email = str_replace('src="','src="'.base_url().'boletins/'.$boletin->carpeta.'/',$email);
                    
                    $email = str_replace('{link}',base_url().'boletins/'.$boletin->carpeta.'/'.$boletin->fichero_html,$email);                    
                    foreach($subscritos->result() as $s){
                        $texto = str_replace('{baja}',base_url('paginas/frontend/unsubscribe/'.$s->email),$email);                    
                        correo($s->email,$boletin->titulo,$texto);
                        echo "Correo enviado a <span style='color:blue'>".$s->email.'</span><br/>';
                    }
                    echo $email;
                }
            }
        }
    }
?>
