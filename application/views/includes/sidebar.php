<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(                        
                        'reservas'=>array('admin/formularios'),
                        'proyectos'=>array('admin/categorias_proyectos','admin/proyectoss'),
                        'seminarios'=>array('admin/categorias_seminarios','admin/seminarioss'),
                        'notificaciones'=>array('admin/notificaciones','admin/subscritos','admin/emails'),
                        'b'=>array('blog_categorias','blog'),                        
                        'paginas'=>array('admin/paginas','admin/subscriptores','admin/curriculums','admin/eventos_banner'),
                        'seguridad'=>array('acciones','ajustes','grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(      
                        'seminarioss'=>array('Seminarios','fa fa-calendar'),
                        'proyectoss'=>array('Proyectos','fa fa-folder'),
                        'b'=>array('Blog','fa fa-book'),                        
                        'paginas'=>array('Paginas','fa fa-file-powerpoint-o'),                        
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#222222; font-size:8px; text-align:center">
            <a href="#" style="color:white;">EVA software</a>
        </div>
        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
