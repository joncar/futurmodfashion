<div class="page-title parallax parallax4" style=" background-position: 50% 49px; background-image: url(../../img/parallax/bg-parallax4_6.jpg);  background-size: inherit;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading style1">
                    <h2 class="title">Detall Noticia</h2>
                </div><!-- /.page-title-heading -->
                <div class="breadcrumbs style1">
                    <ul>
                        <li class="home"><a href="#">Home</a></li>
                        <li class="home"><a href="#">Blog</a></li>
                        <li>Noticia</li>
                    </ul>                   
                </div><!-- /.breadcrumbs --> 
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title parallax -->

<section class="main-content blog-post v1">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="post-wrap">
                    <article class="post">
                        <div class="entry-image">
                            <img src="<?= $detail->foto ?>" alt="image">
                        </div>
                        <div class="content-post">
                            <div class="title-education">
                                <p><?= $detail->categorias->row(0)->blog_categorias_nombre ?></p>
                            </div>
                            <h4 class="title-post">
                                <a href="#"><?= $detail->titulo ?></a>
                            </h4>
                            <div class="entry-meta">                              
                                <span class="author"><a href="#"><?= $detail->user ?></a></span>
                                <span class="date"><a href="#"><?= date("d-m-Y",strtotime($detail->fecha)) ?></a></span>
                                <span class="comment"><a href="#"><?= $comentarios->num_rows() ?> Comentarios</a></span>
                            </div><!-- /.entry-meta -->

                            <div class="entry-content">
                                <?= $detail->texto ?>
                            </div><!-- /entry-post -->

                            <div class="entry-meta style1">                              
                                <span class="tag">
                                    <?php if(!empty($detail->tags)): ?>
                                        <?php foreach(explode(',',$detail->tags) as $t): ?>
                                            <a href="<?= base_url('blog?direccion='.str_replace('#','',$t)) ?>"><?= $t ?></a>, 
                                        <?php endforeach ?>            
                                    <?php endif ?>
                                </span>
                            </div><!-- /.entry-meta -->

                            <ul class="flat-socials">
                                <li class="facebook">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $detail->link ?>">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="twitter">
                                    <a href="http://www.twitter.com/share?url=<?= $detail->link ?>">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <!--<li class="google">
                                    <a href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                <li class="linkedin">
                                    <a href="#">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>-->
                            </ul>
                        </div><!-- /content-post -->
                    </article>

                    <article>
                        <div class="comment-post">
                            <div class="comment-list-wrap">
                                <h4 class="title comment-title"><?= $comentarios->num_rows() ?> Comentaris</h4>
                                <ul class="comment-list">                                    
                                    <?php foreach($comentarios->result() as $c): ?> 
                                        <li>
                                            <article class="comment">
                                                <div class="comment-avatar">
                                                    <img src="<?= base_url() ?>img/blog/single/2.png" alt="image">
                                                </div>                  
                                                <div class="comment-detail">
                                                    <div class="comment-meta">
                                                        <p class="comment-author"><a href="#"><?= $c->autor ?></a></p> 
                                                        <p class="comment-date"><a href="#"><?= date('d-m-Y',strtotime($c->fecha)) ?></a></p> 
                                                    </div>
                                                    <p class="comment-body"><?= strip_tags($c->texto) ?></p>
                                                </div><!-- /.comment-detail -->
                                            </article><!-- /.comment -->
                                        </li>
                                    <?php endforeach ?>  
                                    <?php if($comentarios->num_rows()==0): ?>
                                        <li>                  
                                          Sigues el primer en fer un comentari sobre aquesta noticia
                                        </li>
                                    <?php endif ?>
                                </ul><!-- /.comment-list -->
                            </div><!-- /.comment-list-wrap -->

                            <div id="respond" class="comment-respond">
                                <h4 class="title comment-title style1">Enviar un comentari</h4>

                                <form class="flat-contact-form for-full-width" action="<?= base_url('blog/frontend/comentarios') ?>" method="post">
                                    <?php if(!empty($_SESSION['mensaje'])){
                                            echo $_SESSION['mensaje'];
                                            unset($_SESSION['mensaje']);
                                      }?>
                                    <div class="field clearfix">      
                                        <div class="wrap-type-input">                    
                                            <div class="input-wrap name">
                                                <input type="text" name="autor" value="" tabindex="1" placeholder="Nom" id="name" required="">
                                            </div>
                                            <div class="input-wrap email">
                                                <input type="email" name="email"  value="" tabindex="2" placeholder="Email" id="email-contact" required="">
                                            </div>
                                        </div>
                                        <div class="textarea-wrap">
                                            <textarea class="type-input" name="texto" tabindex="3" placeholder="Comentari" id="message-contact" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="submit-wrap">
                                        <input type="hidden" name="blog_id"  value="<?= $detail->id ?>">
                                        <button class="flat-button button-style">Enviar Ara</button>
                                    </div>
                                </form>
                            </div><!-- /#respond -->
                        </div>
                    </article><!-- /post -->
                </div><!-- /post-wrap -->
            </div><!-- /col-md-8 -->

            <div class="col-md-4">
                <div class="sidebar">
                    <div class="widget widget-search">
                        <form action="<?= base_url('blog') ?>" id="searchform" method="get">
                            <div>
                                <input type="text" id="s" name="direccion" class="sss" placeholder="Buscar">
                                <input type="hidden" id="page" name="page" value="<?= !empty($_GET['page'])?$_GET['page']:'1' ?>">
                                <input type="hidden" id="categorias_proyectos_id" name="blog_categorias_id" value="<?= !empty($_GET['blog_categorias_id'])?$_GET['blog_categorias_id']:'' ?>">
                                <input type="submit" value="" id="searchsubmit">
                            </div>
                        </form>
                    </div><!-- /widget-search -->

                    <div class="widget widget-recent-posts">
                        <h5 class="widget-title">Noticies Relacionades</h5>
                        <ul class="recent-posts clearfix">
                            <?php foreach($relacionados->result() as $r): ?>
                            <li>
                                <div class="thumb">
                                    <img src="<?= $r->foto ?>" alt="image" style="width:84px">
                                </div>
                                <div class="text">
                                    <a href="<?= $r->link ?>"><?= $r->titulo ?></a>
                                    <p><?= date("d-m-Y",strtotime($r->fecha)) ?></p>
                                </div>
                            </li>
                            <?php endforeach ?>
                            <?php if($relacionados->num_rows()==0): ?>
                              <li class="item-recent clearfix">
                                No hi ha articles relacionats
                              </li>
                            <?php endif ?>
                        </ul><!-- /popular-news clearfix -->
                    </div><!-- /widget widget-recent-posts -->

                    <div class="widget widget-categories">
                        <h5 class="widget-title">Categories</h5>
                        <ul class="categories">

                            <?php if($categorias->num_rows()==0): ?>
                                <li>
                                    <a href="#">No hi ha categories</a>
                                </li>
                            <?php endif ?>
                            <?php foreach($categorias->result() as $c): ?>
                                <li><a href="javascript:changeCategoria(<?= $c->id ?>)"><?= $c->blog_categorias_nombre ?> (<?= $c->cantidad ?>)</a></li>
                            <?php endforeach ?>

                        </ul>
                    </div><!-- /widget-categories -->

                    <div class="widget widget-tags">
                        <h5 class="widget-title">Tags</h5>
                        <ul class="tag-list">
                            <?php if(!empty($detail->tags)): ?>
                                <?php foreach(explode(',',$detail->tags) as $t): ?>
                                    <li><a href="<?= base_url('blog?direccion='.str_replace('#','',$t)) ?>" class="tag-link"><span><?= $t ?></span></a></li>
                                <?php endforeach ?>            
                            <?php endif ?>
                        </ul>
                    </div><!-- /widget-tags -->


                </div><!-- /sidebar -->
            </div><!-- /col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /main-content blog-post -->
<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchForm").submit();
    }
    
    function changeCategoria(id){
        $("#blog_categorias_id").val(id);
        $("#searchForm").submit();
    }
</script>
