<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function categorias_fotos(){
            $crud = $this->crud_function('','');
            $crud->add_action('<i class="fa fa-image"></i> Fotos','',base_url('catalogo/admin/fotos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function fotos($id){
            $crud = $this->crud_function('','');
            $crud->field_type('categorias_fotos_id','hidden',$id);
            $crud->field_type('colores','tags');
            $crud->where('categorias_fotos_id',$id);
            $crud->unset_columns('categorias_fotos_id');
            $crud->set_field_upload('foto','img/catalogo');
            $crud = $crud->render();
            $this->loadView($crud);
        }               
    }
?>
