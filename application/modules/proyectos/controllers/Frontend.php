<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();        
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }
        
        function get_categorias(){
            $this->db->limit('8');
            $categorias = $this->db->get_where('categorias_proyectos');
            foreach($categorias->result() as $n=>$c){
                $categorias->row($n)->cantidad = $this->db->get_where('proyectos',array('categorias_proyectos_id'=>$c->id))->num_rows();
            }
            return $categorias;
        }                
        
        public function populares(){
            $proyectos = new Bdsource();
            $proyectos->limit = array('2','0');
            $proyectos->init('proyectos',FALSE,'populares');
            foreach($this->populares->result() as $n=>$b){
                $this->populares->row($n)->link = site_url('projectes/'.toURL($b->id.'-'.$b->titulo));
                $this->populares->row($n)->foto = base_url('img/proyectos/'.$b->foto);
            }
            foreach($this->populares->result() as $n=>$b){
                $this->populares->row($n)->comentarios = $this->db->get_where('comentarios_proyectos',array('proyectos_id'=>$b->id))->num_rows();                
            }
            return $this->populares;
        }
        
        
        
        function index($anio = ''){ 
            $this->db->query('SET NAMES utf8');
            $crud = new ajax_grocery_CRUD();
            $crud->set_table('proyectos');
            $crud->set_subject('Proyectos');
            $crud->set_theme('crud');
            if(!empty($_GET['b'])){
                $crud->like('titulo',$_GET['b']);
                $crud->or_like('empresa',$_GET['b']);
            }
            $crud->set_url('proyectos/frontend/index/');
            $crud->columns('id','link','categorias_proyectos_id','foto','titulo','texto','likes','visitas');
            $crud->set_relation('categorias_proyectos_id','categorias_proyectos','nombre');
            $crud->callback_column('link',function($val,$row){
                return site_url('projectes/'.toURL($row->id.'-'.$row->titulo));                        
            });
            $crud->callback_column('foto',function($val){
                return base_url().'img/proyectos/'.$val;                        
            });
            $crud->callback_column('likes',function($val){
                return count(json_decode($val));
            });
            $anio = empty($anio)?date("Y"):$anio;
            if(is_numeric($anio)){
                $crud->where('anio',$anio);
            }
            if(empty($_POST)){
                $crud->limit(9);
            }
            $crud->where('proyectos.status',1);
            $crud->order_by('id','DESC');
            $crud->set_url('proyectos/frontend/index/'.$anio.'/');
            $crud = $crud->render('','application/modules/proyectos/views');
            $crud->title = 'Proyectos';
            $crud->crud = 'user';
            $crud->view = 'crud/main';
            $crud->populares  = $this->populares();
            $crud->categorias = $this->get_categorias();
            $crud->myscripts = get_header_crud($crud->css_files,$crud->js_files,TRUE);
            $crud->anio = $anio;
            //$crud->scripts = 'a';
            $this->loadView($crud);
        }
        
        public function read($id,$hash = ''){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $proyectos = new Bdsource();
                $proyectos->where('id',$id);
                $proyectos->init('proyectos',TRUE);
                $this->proyectos->link = site_url('proyectos/'.toURL($this->proyectos->id.'-'.$this->proyectos->titulo));
                $this->proyectos->foto = base_url('img/proyectos/'.$this->proyectos->foto);
                $this->proyectos->user = $this->db->get_where('user',array('id'=>$this->proyectos->user_id));
                $this->proyectos->user = $this->proyectos->user->num_rows()>0?$this->proyectos->user->row()->nombre:'Anonimo';
                $this->proyectos->categorias = $this->db->get_where('categorias_proyectos',array('id'=>$this->proyectos->categorias_proyectos_id));
                $this->proyectos->likes = count(json_decode($this->proyectos->likes));
                if(empty($hash) || 's2w'.substr(md5($id),0,8)!=$hash){
                    $this->proyectos->texto = $this->proyectos->descripcion_inicial.'<p align="center"><a href="'.base_url('p/contacto').'?proyecto='.$id.'"><button type="button" class="flat-button button-style" style="margin-top:30px;">Vols veure més informació d’aquest projecte?</button></a></p>';
                }
                $this->db->limit('5');
                $this->db->order_by('orden','ASC');
                $this->proyectos->fotos = $this->db->get_where('proyectos_fotos',array('proyectos_id'=>$this->proyectos->id));
                if($proyectos->num_rows()>0){
                    $proyectos->visitas++;
                    $proyectos->save();
                }else{
                    throw new Exception("El proyecto que desea mirar, ha sido eliminado o no existe.",404);
                    die();
                }
                $comentarios = new Bdsource();
                $comentarios->where('proyectos_id',$this->proyectos->id);
                $comentarios->init('comentarios_proyectos',FALSE,'comentarios');
                $relacionados = new Bdsource();
                $relacionados->limit = array(4); 
                $relacionados->where('categorias_proyectos_id',$this->proyectos->categorias_proyectos_id);
                $relacionados->where('id !=',$this->proyectos->id);
                $relacionados->order_by = array('id','desc');
                $relacionados->init('proyectos',FALSE,'relacionados');
                foreach($this->relacionados->result() as $n=>$b){
                    $this->relacionados->row($n)->link = site_url('projectes/'.toURL($b->id.'-'.$b->titulo));
                    $this->relacionados->row($n)->foto = base_url('img/proyectos/'.$b->foto);
                }
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->proyectos,
                        'title'=>$this->proyectos->titulo,
                        'comentarios'=>$this->comentarios,
                        'categorias'=>$this->get_categorias(),
                         'populares'=>$this->populares(),
                        'relacionados'=>$this->relacionados
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function comentarios(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('autor','Autor','required')
                                  ->set_rules('texto','Comentario','required')
                                  ->set_rules('proyectos_id','','required|numeric');
            if($this->form_validation->run()){
                $data = array();
                foreach($_POST as $n=>$p){
                    $data[$n] = $p;
                }
                $data['fecha'] = date("Y-m-d");
                $this->db->insert('comentarios_proyectos',$data);
                $_SESSION['mensaje'] = $this->success('Comentario añadido con éxito <script>document.reload();</script>');
                header("Location:".base_url('proyectos/frontend/read/'.$_POST['proyectos_id'].'#respond'));
            }else{
                $_SESSION['mensaje'] = $this->error('Comentario no enviado con éxito');
                header("Location:".base_url('proyectos/frontend/read/'.$_POST['proyectos_id'].'#respond'));                
            }
        }
        
        function sumarCorazon($id){
            $proyecto = $this->db->get_where('proyectos',array('id'=>$id));
            if($proyecto->num_rows()>0){
                $likes = empty($proyecto->row()->likes)?array():json_decode($proyecto->row()->likes);                           
                $ip = getUserIP();                
                if(!in_array($ip,$likes)){
                    $likes[] = $ip;
                }else{
                    foreach($likes as $n=>$v){
                        if($v==$ip){
                            unset($likes[$n]);
                        }
                    }
                }
                $count = count($likes);
                $this->db->update('proyectos',array('likes'=>json_encode($likes)),array('id'=>$id));
                echo $count;
            }else{
                echo 'fail';
            }
        }
    }
?>
