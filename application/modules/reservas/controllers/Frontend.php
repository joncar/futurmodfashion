<?php 
    require_once APPPATH.'/controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }
        
        function reservas($x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_CRUD();            
            $crud->set_theme('bootstrap2');
            $crud->set_table('reservas');
            $crud->set_subject('Reservas');
            $crud->required_fields_array();
            //$crud->set_rules('email','Email','required|valid_email|callback_validar_invitacion');
            $crud->set_rules('email','Email','required|valid_email');
            $crud->callback_after_insert(function($post){
                $formulario = get_instance()->db->get_where('formularios',array('id'=>$_POST['formularios_id']));
                if($formulario->num_rows()>0){
                    $post['fecha'] = strftime('%d %B %Y',strtotime($formulario->row()->fecha_cierre));
                }
                get_instance()->enviarcorreo($post,9,'info@futurmod.fashion');
            });            
            $crud = $crud->render();                        
        }

        function validar_invitacion(){
            $formulario = $this->db->get_where('reservas',array('email'=>$_POST['email'],'formularios_id'=>$_POST['formularios_id']));
            if($formulario->num_rows()>0){
                $this->form_validation->set_message('validar_invitacion','Ya se ha registrado este correo.');
                return false;
            }
        }

        function formulario($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $formulario = $this->db->get_where('formularios',array('id'=>$id));
                if($formulario->num_rows()>0){
                    $formulario = $formulario->row();
                    $this->loadView(array('view'=>'formularios','id'=>$id,'formulario'=>$formulario));
                }else{
                    throw new exception('El formulario no esta disponible',404);    
                }
            }else{
                throw new exception('El formulario no esta disponible',404);
            }
        }
    }
?>
