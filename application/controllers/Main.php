<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {
    //public $theme = 'themes/bristol/';
    public $theme = '';
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('redsysAPI');
        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_ALL, 'ca_ES', 'Catalan_Spain', 'Catalan');
    }
    
    
    
    function get_entries(){
        $blog = new Bdsource();
        $blog->limit = array('3','0');
        $blog->order_by = array('fecha','DESC');        
        $blog->init('blog');
        foreach($this->blog->result() as $n=>$b){
            $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
            $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
            $this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
        }
        if($this->blog->num_rows()>0){
            $this->blog->tags = $this->blog->row()->tags;
        }
    }
    
     function proyectos(){
        $proyectos = new Bdsource();
        $proyectos->limit = array('9','0');
        $proyectos->order_by = array('id','DESC');        
        $proyectos->where('anio',date("Y"));
        $proyectos->where('status',1);
        $proyectos->init('proyectos');        
        foreach($this->proyectos->result() as $n=>$b){
            $this->proyectos->row($n)->link = site_url('projectes/'.toURL($b->id.'-'.$b->titulo));
            $this->proyectos->row($n)->foto = base_url('img/proyectos/'.$b->foto);
            $this->proyectos->row($n)->comentarios = $this->db->get_where('comentarios_proyectos',array('proyectos_id'=>$b->id))->num_rows();                
            $this->proyectos->row($n)->categorias = $this->db->get_where('categorias_proyectos',array('id'=>$b->categorias_proyectos_id));
            $this->proyectos->row($n)->likes = count(json_decode($this->proyectos->row($n)->likes));
        }
        if($this->proyectos->num_rows()>0){
            $this->proyectos->tags = $this->proyectos->row()->tags;
        }
    }
    
    function seminarios_viejos(){
        $seminarios_viejos = new Bdsource();
        $seminarios_viejos->limit = array('9','0');
        $seminarios_viejos->order_by = array('fecha','ASC'); 
        $seminarios_viejos->db->where('fecha <',date("Y-m-d"));
        $seminarios_viejos->init('seminarios',FALSE,'seminarios_viejos');
        foreach($this->seminarios_viejos->result() as $n=>$b){
            $this->seminarios_viejos->row($n)->link = site_url('seminaris/'.toURL($b->id.'-'.$b->titulo));
            $this->seminarios_viejos->row($n)->foto = base_url('img/seminarios/'.$b->foto);
            $this->seminarios_viejos->row($n)->comentarios = $this->db->get_where('comentarios_seminarios',array('seminarios_id'=>$b->id))->num_rows();                
            $this->seminarios_viejos->row($n)->categorias = $this->db->get_where('categorias_seminarios',array('id'=>$b->categorias_seminarios_id));
        }
        if($this->seminarios_viejos->num_rows()>0){
            $this->seminarios_viejos->tags = $this->seminarios_viejos->row()->tags;
        }
    }
    
     function seminarios(){
        $seminarios = new Bdsource();
        $seminarios->limit = array('9','0');
        $seminarios->order_by = array('fecha','ASC'); 
        $seminarios->db->where('fecha >=',date("Y-m-d"));
        $seminarios->where('anio',date("Y"));
        $seminarios->init('seminarios');        
        foreach($this->seminarios->result() as $n=>$b){
            $this->seminarios->row($n)->link = site_url('seminaris/'.toURL($b->id.'-'.$b->titulo));
            $this->seminarios->row($n)->foto = base_url('img/seminarios/'.$b->foto);
            $this->seminarios->row($n)->comentarios = $this->db->get_where('comentarios_seminarios',array('seminarios_id'=>$b->id))->num_rows();                
            $this->seminarios->row($n)->categorias = $this->db->get_where('categorias_seminarios',array('id'=>$b->categorias_seminarios_id));
        }
        if($this->seminarios->num_rows()>0){
            $this->seminarios->tags = $this->seminarios->row()->tags;
        }
    }

    public function index() {
        $this->get_entries();        
        $this->proyectos();        
        $this->seminarios(); 
        $this->seminarios_viejos();       
        $this->loadView(array('view'=>'main','blog'=>$this->blog,'proyectos'=>$this->proyectos,'seminarios_viejos'=>$this->seminarios_viejos,'seminarios'=>$this->seminarios));
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']) || $this->user->admin==1)
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $view = $this->load->view('template', $param,TRUE);
        $ajustes = $this->db->get_where('ajustes')->row();
        if(!empty($ajustes->analytics)){
            $view= str_replace('</body>',$ajustes->analytics.'</body>',$view);
        }
        echo $view;
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }            
        }

}
