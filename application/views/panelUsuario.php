<div class="page-title parallax parallax4 panel"  style=' background-size: inherit;'>           
        <div class="container">
            <div class="row">
                <div class="col-md-12">                    
                    <div class="page-title-heading">
                        <h2 class="title">Els meus projectes</h2>
                    </div><!-- /.page-title-heading -->
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><a href="#">Home</a></li>
                            <li>Els meus projectes</li>
                        </ul>                   
                    </div><!-- /.breadcrumbs --> 
                </div><!-- /.col-md-12 -->  
            </div><!-- /.row -->  
        </div><!-- /.container -->                      
    </div><!-- /page-title parallax -->

    <section class="main-content blog-post v1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if($this->db->get('ajustes')->row()->aceptar_proyectos==0): ?>
                        <div class="alert alert-warning">
                            <i class="fa fa-exclamation-triangle"></i> Ho sentim ja no ens podem acceptar més projectes fins a la proxima convocatoria, si vols que t’avisem quan tornem a acceptar projectes, envians un <a href="<?= base_url('p/contacto') ?>">formulari de contacte</a> i nosaltres t’avisem. Perdoneu les molesties.
                        </div>
                    <?php endif ?>
                    <?= $output ?>
                </div><!-- /col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->   
    </section><!-- /main-content blog-post -->
