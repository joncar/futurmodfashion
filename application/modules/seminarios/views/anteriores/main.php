<div class="page-title parallax parallax4 projectes"  style=' background-size: inherit;'>           
        <div class="container">
            <div class="row">
                <div class="col-md-12">                    
                    <div class="page-title-heading">
                        <h2 class="title">Presentacions</h2>
                    </div><!-- /.page-title-heading -->
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><a href="<?= site_url() ?>">Home</a></li>
                            <li>Presentacions</li>
                        </ul>                   
                    </div><!-- /.breadcrumbs --> 
                </div><!-- /.col-md-12 -->  
            </div><!-- /.row -->  
        </div><!-- /.container -->                      
    </div><!-- /page-title parallax -->

    <section id="projectess" class="flat-row flat-recent-causes recent-mag-top pad-bottom50px">
        <div class="container">            
            <div class="row">
                <div style="text-align:center" class="col-xs-12 col-sm-9">Per veure les presentacions del Seminaris t’has de registrar. Gràcies</div>
                <div class="col-xs-12 col-sm-3">
                    <select class="form-control" id="changeYear">
                        <?php 
                            $this->db->select('MIN(YEAR(fecha)) as anio');
                            $seminarios_viejos = $this->db->get('seminarios')->row()->anio;
                            for($i=date("Y");$i>=$seminarios_viejos;$i--):
                        ?>
                        <option <?= $anio==$i?'selected':'' ?>><?= $i ?></option>
                        <?php endfor ?>                        
                    </select>
                </div>
            </div>
            <div class="row">
                <?= $output ?>
            </div><!-- /.row -->
        </div><!-- /.container -->   
    </section><!-- /main-content blog-post -->
<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchform").submit();
    }
    
    function changeCategoria(id){
        $("#blog_categorias_id").val(id);
        $("#searchform").submit();
    }
    
    function savePresentacion(id){
        $.post('<?= base_url('seminarios/frontend/presentacions/save') ?>/'+id,{},function(data){            
            var data = JSON.parse(data);
            if(data.success){                                
                $("#buttonid"+id).html('<i class="fa fa-heart-o"></i> guardar presentacio');
                document.location.href="<?= base_url('presentacions') ?>";
            }else{                
                $("#buttonid"+id).html('guardar presetancio');
                $.post('<?= base_url("seminarios/frontend/presentacions/ajax_list_info") ?>',{},function(data){
                    var data = JSON.parse(data);
                    $("#presentacionscount").html(data.total_results);
                });
            }
            $("#page").val(1);
            <?php if($this->router->fetch_method()=='presentacions'): ?>
                $(".ajax_list").html('');
                $('.filtering_form').trigger('submit');
            <?php endif ?>            
        });
    }

    $(document).on('change','#changeYear',function(){
        document.location.href="<?= base_url('seminaris-any') ?>/"+$(this).val()
    });
</script>
